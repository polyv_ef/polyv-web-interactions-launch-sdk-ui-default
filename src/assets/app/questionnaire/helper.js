export const maxOptionsCount = 10; // 每道题目的最大选项数量
export const minOptionsCount = 2; // 每道题目的最小选项数量
export const maxQuestionsCount = 100; // 单份问卷的最大题目数量

// 新版问卷默认参数
export const STEP = {
  QUESTIONNAIRE_LIST: 'questionnaireList', // 问卷列表
  // QUESTIONNAIRE_EDIT: 'questionnaireCreate', // 问卷详情
  QUESTIONNAIRE_EDIT: 'questionnaireEdit', // 新建和编辑问卷
  QUESTIONNAIRE_STATISTICS: 'questionnaireStatistics', // 问卷统计
  QUESTIONNAIRE_ALL_PREVIEW: 'questionnaireAllPreview', // 问卷全面预览页
};

export const PUBLISH_TYPE = {
  AUTO: 'auto',
  MANUAL: 'manual'
};

export const QUESTIONNAIRE_TYPE = {
  // 问卷调研
  SURVEY: 'survey',
  // 随堂测试
  EXAM: 'exam',
  EVALUATION: 'evalution'
};

// 题目类型
export const QUESTION_TYPE = {
  // 单选题
  SINGLE: 'R',
  // 多选题
  MULTIPLE: 'C',
  // 问答题
  TEXT: 'Q',
  // 判断题
  JUDGE: 'J',
  // 评分题(星星)
  STAR: 'X',
  // 评分题(数字)
  STAR_NUMBER: 'S',
  // 多项得分题
  MULTIPLE_SCORE: 'D'
};

export const maxLen = {
  option: 400, // 选项内容最大长度
  title: 60, // 问卷标题最大长度
  questionTitle: 500, // 题目内容最大长度
  questionTitleMax: 1000, // 长文本题目内容最大长度
};

export const STATUS = {
  NOT_SENDED: 'saved', // 未发布
  SENDED: 'published', // 已发布
  FINISHED: 'forbidden', // 已终止
};

/**
 * 根据 index 返回对应的小写字母
 */
export function toLowercaseLetter(index) {
  return String.fromCharCode(97 + index);
}

/**
 * 根据 index 返回对应的大写字母
 */
export function toUppercaseLetter(index) {
  return String.fromCharCode(65 + index);
}

export function getQuestionnaireModel(options = {}) {
  const questionOptions = {};
  if (options.questionnaireType === QUESTIONNAIRE_TYPE.EXAM) {
    questionOptions.isScorable = true;
  }
  const firstQuestion = getQuestionModel('R', questionOptions);
  return Object.assign({
    name: '',
    desc: '',
    questionnaireId: '',
    questions: [firstQuestion],
    questionnaireType: QUESTIONNAIRE_TYPE.SURVEY,
    timeLimit: '',
    privacy: {
      ...getPrivacyModel()
    }
  }, options);
}

export function getQuestionModel(type = 'R', options) {
  const question = {
    questionId: Date.now() + '',
    desc: '',
    name: '',
    type,
    answer: '',
    isRequired: true,
    isScorable: false,
    score: '',
    scoreExt: {
      option1: 0,
      option2: 0
    },
    options: ['', '']
  };
  if (type === QUESTION_TYPE.STAR) {
    question.star = 0;
    question.options = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
  }
  if (type !== QUESTION_TYPE.MULTIPLE_SCORE) {
    question.scoreExt = null;
  }
  return Object.assign(question, options);
}

/**
 * 某个选项是否为正确选项。
 * @param {String} answer - 所有正确选项的小写字母字符串集合，如 'ab'。
 * @param {Number} optionIndex - 选项序号，从 0 开始。
 * @returns 该选项是否正确。
 */
export function isAnswer(answer = '', optionIndex = -1) {
  const optionLetter = toLowercaseLetter(optionIndex);
  return answer.indexOf(optionLetter) > -1;
}

/**
 * 计算选项的选择百分比。
 * @param {Object} statistics - 统计结果。包括所有提交人数（total: Number）和各选项的提交人数（options: Array<number>）。
 * @param {Number} optionIndex - 选项序号，从 0 开始。
 * @returns 选择该选项占总人数的比例，返回带百分号的字符串，精确到小数点后两位。如果是整数，忽略小数位。
 */
export function getOptionPercentageStr(statistics, optionIndex) {
  let percentageStr = '';
  if (statistics && statistics.total) {
    const percentage = (statistics.options[optionIndex] / statistics.total) || 0;
    percentageStr = `${(percentage * 100).toFixed(2).replace('.00', '')}%`;
  }
  return percentageStr;
}

/**
 * 计算平均分。
 * @param {Number} totalScore 总分。
 * @param {Number} totalPerson 总人数。
 * @returns 表示平均分的字符串，精确到小数点后两位。如果是整数，忽略小数位。
 */
export function getAverageScore(totalScore, totalPerson) {
  const averageScore = totalPerson ? totalScore / totalPerson : 0;
  return parseFloat(averageScore).toFixed(2).replace('.00', '');
}

// 问卷隐私协议数据
export function getPrivacyModel() {
  return {
    content: '',
    required: 'N'
  };
}

// 多项分数转换为字符串格式
export function formatScoreExt(data) {
  let scoreExt = '{';
  const dataArray = Object.keys(data);
  dataArray.forEach((option, index) => {
    scoreExt += `"${option}": ${data[option]}`;
    if (index < dataArray.length - 1) scoreExt += ',';
  });
  scoreExt += '}';
  return scoreExt;
}

// 转化为提交问卷的数据
export function parseSaveQuestionnaire(questionnaire) {
  return {
    autoEndTime: questionnaire.autoEndTime,
    autoPublishTime: questionnaire.autoPublishTime,
    ossEnabled: questionnaire.ossEnabled,
    type: questionnaire.questionnaireType,
    timeLimit: questionnaire.timeLimit,
    name: questionnaire.name,
    desc: questionnaire.desc || '',
    questionnaireId: questionnaire.questionnaireId,
    privacyContent: questionnaire.privacy ? questionnaire.privacy.content : '',
    privacyEnabled: questionnaire.privacy ? questionnaire.privacy.required : '',
    scoreLabelEnabled: questionnaire.scoreLabelEnabled ? questionnaire.scoreLabelEnabled : '',
    questionnaireScoreLabels: questionnaire.questionnaireScoreLabels || [],
    questions: questionnaire.questions.map((question) => {
      return {
        answer: question.answer,
        name: question.name,
        desc: question.desc || '',
        isRequired: question.isRequired,
        isScorable: question.isScorable,
        score: question.score !== '' ? Number(question.score) : '',
        type: question.type,
        options: question.options,
        scoreExt: question.scoreExt ? formatScoreExt(question.scoreExt) : null
      };
    })
  };
}
