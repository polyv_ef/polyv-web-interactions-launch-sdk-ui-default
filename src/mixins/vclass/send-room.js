
export default {
  props: {
    /** 研讨会 发送人群列表 */
    sendRoomList: {
      type: Array,
      default() {
        return [];
      }
    },
    /** 研讨会 选择人群弹窗标题 */
    sendRoomTitle: {
      type: String,
      default: '提示',
    },

    /** 研讨会 选择人群弹窗确认按钮 */
    sendRoomConfirmText: {
      type: String,
      default: '立即发布',
    },
  },
  data() {
    return {
      showSendRoomModal: false,
    };
  },
  methods: {
    // 获取发送人群参数
    getSendTargetParam(sendTarget) {
      if (!sendTarget) return;
      sendTarget = this.sendRoomList.filter(item => item.checked === true);
      // 过滤sendTarget 非type字段
      sendTarget = sendTarget.map((item) => {
        return {
          type: item.type
        };
      });
      return sendTarget;
    },
  }
};
