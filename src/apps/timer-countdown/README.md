# Timer 计时器挂件

## 引入组件

```js
import Timer from '@polyv/interactions-launch-sdk-ui-default/lib/Timer';
```



## 代码示例

```vue
<template>
	<button @click="timerVisible = true">计时器</button>
  <div id="pane" class="c-interactions-pane">
    <template v-show="timerVisible">
      <TimerCountdown
        v-if="timerCore"
        :lang="lang"
        :core="timerCore"
        @close="handleTimerClose"
        @update-visible="timerVisible = $event"
      />
    </template>
  </div>
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import TimerCountdown from '@polyv/interactions-launch-sdk-ui-default/lib/TimerCountdown';

export default {
  components: {
    TimerCountdown,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',
      // 计时器 SDK 实例
      timerCore: null,
      timerVisible: false,
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.timerCore = new InteractionsLaunchSDK.Timer();
  },
  methods: {
    // 关闭计时器需要二次确认
    handleTimerClose(isOngoing) {
      if (!isOngoing) {
        this.timerVisible = false;
        return;
      }
      if (window.confirm(this.$t('timerCloseTips'))) {
        this.timerVisible = false;
      }
    },
  }
};
</script>
```



## Attributes

| 属性名     | 类型          | 默认值  | 含义                                                         |
| ---------- | ------------- | ------- | ------------------------------------------------------------ |
| core       | object        | null | 计时器 SDK 实例                                              |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |
| showClose | boolean | true | 是否显示关闭按钮 |



## Events

| 事件名         | 说明                                                 | 回调参数类型 | 回调参数含义           |
| -------------- | ---------------------------------------------------- | ------------ | ---------------------- |
| close          | 点击弹窗关闭按钮时触发该事件。关闭弹窗后将停止计时。 | boolean      | 是否还在进行计时。     |
| update-visible | 有正在进行的计时器时或计时器结束时会触发该事件。                   | boolean      | 当前弹窗是否应该可见。 |
