import { formatDate } from '@polyv/utils/src/date';
import Locale from '@/mixins/locale';
import commonMixin from '../app-mixin';

export default {
  mixins: [commonMixin, Locale],

  props: {
    /** 抢答 SDK 实例 */
    core: {
      type: Object,
      default: null,
    },
    /** 邀请上麦 */
    showInviteToMic: Boolean,
  },

  computed: {
    isShowRecord() {
      return this.showRecord || this.showRecordDetail;
    },
  },

  data() {
    return {
      status: 'preStart',
      overTime: 0,
      answerNum: 0,
      maxNumber: 0,
      rushAnswerId: '',
      statistics: [],
      NETWORK_ERR: this.t('plvIA.common.networkErr'),
      showEndTip: false,
      showRecord: false,
      showRecordDetail: false,
      listRecord: [],
      listRank: [],
      listRecordDetail: [],
      loadingRankList: false,
      recordPageNumber: 1,
      recordTotalPages: 1,
      recordRequesting: false,
      recordDetailPageNumber: 1,
      recordDetailTotalPages: 1,
      recordDetailRequesting: false,
    };
  },

  timer: 0,

  beforeDestroy() {
    this.clearTimer();
    this.unbindChatEvents();
  },

  methods: {
    async afterCoreInited() {
      this.bindChatEvents();
      await this.updateStatus();
    },

    async start(limitTime, maxNumber, remark) {
      const { overTime, rushAnswerId: quicklyId, maxNumber: responseMaxNumber } = await this.core.start(limitTime, maxNumber, remark);
      this.handleStart({
        overTime,
        quicklyId,
        maxNumber: responseMaxNumber,
      });
    },

    handleStart({ overTime, quicklyId, maxNumber }) {
      this.overTime = overTime;
      this.rushAnswerId = quicklyId;
      this.maxNumber = maxNumber;
      this.status = 'inAnswer';
      this.updateStatus();
    },

    clearTimer() {
      if (!this.$options.timer) return;
      clearInterval(this.$options.timer);
      this.$options.timer = 0;
    },

    formatListTime(list, timeKey = 'timestamp') {
      return list.map((item, index) => {
        item.formatTime = formatDate(item[timeKey], 'YYYY-MM-DD hh:mm:ss');
        item.ranking = index + 1;
        return item;
      });
    },

    async getRankList() {
      if (this.loadingRankList) return;
      this.loadingRankList = true;
      try {
        const list = await this.core.getRankList(this.rushAnswerId);
        this.listRank = this.formatListTime(list);
        this.answerNum = this.listRank.length;
      } catch (error) {
        console.warn(error);
      }
      this.loadingRankList = false;
    },

    // 获取抢答状态
    async updateStatus() {
      this.clearTimer();
      try {
        const { ongoingStatus, overTime, answerNum, rushAnswerId, maxNumber } = await this.core.getStatus();
        if (ongoingStatus) {
          this.status = 'inAnswer';
          this.overTime = overTime;
          this.answerNum = answerNum;
          this.rushAnswerId = rushAnswerId;
          this.maxNumber = maxNumber;
          this.getRankList();
          this.$options.timer = setTimeout(this.updateStatus, 1000);
        } else {
          this.overTime = 0;
          if (this.status === 'inAnswer') {
            this.showEndTip = true;
            setTimeout(() => {
              this.showEndTip = false;
              this.status = 'answerOver';
              // TODO:刷新loading
              this.getRankList();
              // this.getAnswerList();
            }, 2000);
          }
        }
      } catch (error) {
        this.clearTimer();
        if (this.status === 'inAnswer') {
          this.$iaMessage.default(error.message || this.NETWORK_ERR);
        }
      }
    },

    // 主动结束抢答
    stop() {
      this.core.stop(this.rushAnswerId);
    },

    handleToPreStart() {
      this.status = 'preStart';
      this.$refs.preStart.reset();
    },

    async getAnswerList() {
      try {
        const answerList = await this.core.getAnswerList(this.rushAnswerId);
        this.statistics = answerList.sort((a, b) => a.useTime - b.useTime);
      } catch (error) {
        this.$iaMessage.default(error.message || this.NETWORK_ERR);
      }
    },

    async handleRecord() {
      if (this.recordPageNumber > this.recordTotalPages || this.recordRequesting) return Promise.resolve([]);
      this.recordRequesting = true;
      try {
        const res = await this.core.getRecordList({ pageNumber: this.recordPageNumber });
        this.recordPageNumber = res.pageNumber + 1;
        this.recordTotalPages = res.totalPages;
        this.listRecord = this.listRecord.concat(res.contents.map(item => {
          item.formatTime = formatDate(item.startTime, 'YYYY-MM-DD hh:mm:ss');
          return item;
        }));
      } catch (error) {
        console.warn(error);
      }
      this.showRecord = true;
      this.recordRequesting = false;
    },

    handleScrollBottom() {
      this.handleRecord();
    },

    async handleCheckRecordDetail(data) {
      if (this.recordDetailPageNumber > this.recordDetailTotalPages || this.recordDetailRequesting) return Promise.resolve([]);
      this.recordDetailRequesting = true;
      try {
        // TODO:目前详情最多只有20条，一次性请求回来，后续如果改为无限制，则需要进行滚动加载下一页
        const res = await this.core.getRecordDetailList(data.answerId, this.recordDetailPageNumber, 20);
        this.recordDetailPageNumber = res.pageNumber + 1;
        this.recordDetailTotalPages = res.totalPages;
        this.listRecordDetail = this.listRecordDetail.concat(res.contents.map(item => {
          item.formatTime = formatDate(item.answerTime, 'YYYY-MM-DD hh:mm:ss');
          return item;
        }));
      } catch (error) {
        console.warn(error);
      }
      this.showRecord = false;
      this.showRecordDetail = true;
      this.recordDetailRequesting = false;
    },

    backToMain() {
      this.showRecord = false;
      this.listRecord = [];
      this.recordPageNumber = 1;
      this.recordTotalPages = 1;
    },

    backToRecord() {
      this.showRecordDetail = false;
      this.showRecord = true;
      this.listRecordDetail = [];
      this.recordDetailPageNumber = 1;
      this.recordDetailTotalPages = 1;
    },

    bindChatEvents() {
      this.core.on('START_ANSWER_QUICKLY', this.onStartAnswerQuicklyMsg);
      this.core.on('joinSuccess', this.handleVideoCallStatus);
      this.core.on('joinLeave', this.handleVideoCallStatus);
    },

    unbindChatEvents() {
      this.core.off('START_ANSWER_QUICKLY', this.onStartAnswerQuicklyMsg);
      this.core.off('joinSuccess', this.handleVideoCallStatus);
      this.core.off('joinLeave', this.handleVideoCallStatus);
    },

    onStartAnswerQuicklyMsg(msg) {
      this.handleStart(msg);
    },

    handleVideoCallStatus(msg) {
      const index = this.listRank.findIndex(item => item.userId === msg.user.loginId);
      if (index > -1) {
        const target = this.listRank[index];
        target.voice = msg.EVENT === 'joinSuccess' ? 1 : 0;
        this.listRank.splice(index, 1, target);
      }
    },
  },
};
