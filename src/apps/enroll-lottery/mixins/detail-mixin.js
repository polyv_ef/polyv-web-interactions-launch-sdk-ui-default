/**
 * @file 设置抽奖相关信息
 */
import { formatDate } from '@polyv/utils/src/date';
export default {
  data() {
    return {
      historyPrize: [],
    };
  },

  methods: {
    getPrize(val) {
      if (val) {
        this.prize = val;
        this.wrongOptions.prize = false;
      }
    },

    checkNumber(value) {
      if (Number(value) === 0) {
        this.wrongOptions.amount = true;
        this.amount = '';
        return;
      }
      this.amount = value;
      this.wrongOptions.amount = false;
    },

    optionFocus(refs) {
      this.$nextTick(() => refs.focus());
    },

    // 检查提交参数是否合规
    checkOptions(options) {
      const { name, amount, sourceRange, startTime, endTime } = options;
      if (!name || name === '') {
        this.wrongOptions.prize = true;
        this.optionFocus(this.$refs.prize);
        return false;
      }

      if (!sourceRange.length) {
        this.$iaMessage.default('请选择抽奖名单');
        return false;
      }

      if (sourceRange.indexOf('enroll') !== -1) {
        if (!startTime) {
          this.$iaMessage.default('请选择开始时间');
          return false;
        }

        if (!endTime) {
          this.$iaMessage.default('请选择截止时间');
          return false;
        }
      }

      if (!amount || amount === '' || amount === 0) {
        this.wrongOptions.amount = true;
        this.$refs.numberSelect.inputFocus();
        return false;
      }

      return true;
    },

    getMillisecond(ts1, ts2) {
      // 时间精确到分
      return new Date(`${formatDate(ts1, 'YYYY-MM-DD')} ${formatDate(ts2, 'hh:mm')}:00`).getTime();
    },

    async saveLotteryInfo() {
      const {
        lotteryId,
        prize,
        localListEnabled,
        enrollListEnabled,
        file,
        enrollStartDate,
        enrollStartTime,
        enrollEndDate,
        enrollEndTime,
        autoEndLottery,
        amount,
        isFilterWinner,
        onlineUserEnabled,
      } = this;
      const sourceRange = [];
      if (localListEnabled) {
        sourceRange.push('local');
      }
      if (enrollListEnabled) {
        sourceRange.push('enroll');
      }

      const options = {
        name: prize,
        sourceRange,
        filterWinner: isFilterWinner ? 'Y' : 'N',
        lotteryType: autoEndLottery ? 'auto' : 'manual',
        onlineUser: onlineUserEnabled ? 'Y' : 'N',
        amount,
      };
      if (lotteryId) options.lotteryId = lotteryId;
      if (localListEnabled) {
        options.enrollListFile = file;
      }
      if (enrollListEnabled) {
        options.startTime = this.getMillisecond(enrollStartDate, enrollStartTime);
        options.endTime = this.getMillisecond(enrollEndDate, enrollEndTime);
      } else {
        // 当没有开启报名名单类型时，需要将开奖类型（是否自动开奖）设置为手动类型，否则无法成功提交
        options.lotteryType = 'manual';
      }
      const rightOptions = this.checkOptions(options);
      if (!rightOptions) return;

      this.checkPromise && await this.checkPromise;
      if (this.prizeNameError) return;

      this.btnEnabled = true;
      options.sourceRange = sourceRange.join(',');
      this.options = options;
      this.$emit('save', this.options);
    },

    cancelLottery() {
      this.$emit('cancel');
    },
  }
};
