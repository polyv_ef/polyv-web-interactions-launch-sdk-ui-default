# 互动功能发起端 UI 组件

## 概述

本项目是保利威直播互动功能发起端（开播端及管理端等）的 UI 组件，基于 Vue.js 2.6.x 编写，开发人员可以直接使用本组件或基于本组件进行二次开发。

如果您使用的是 React、Angular 等其他框架，可以参照[本组件源代码](https://gitee.com/polyv_ef/polyv-web-interactions-launch-sdk-ui-default)和 [SDK 文档](https://help.polyv.net/index.html#/live/js/new_sdk/interactions_launch_sdk/sdk/overview)自行实现对应的界面。

注意，部分功能在 UI 设计稿中是以模态框（Modal）形式呈现的，但本组件仅提供主体内容部分，请自行实现外层模态框（或以其他形式展现），并根据对应组件的事件去控制可见性。

## 使用文档

### 安装

```bash
npm i @polyv/interactions-launch-sdk @polyv/interactions-launch-sdk-ui-default
```

### 引入 SDK 与组件

```javascript
// 引入 @polyv/interactions-launch-sdk
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
// 按需引入组件
import CheckIn from '@polyv/interactions-launch-sdk-ui-default/lib/CheckIn';
import AnswerCard from '@polyv/interactions-launch-sdk-ui-default/lib/AnswerCard';
import Timer from '@polyv/interactions-launch-sdk-ui-default/lib/Timer';
```

### 引入其他依赖

互动功能发起端 UI 组件依赖于 [Element UI (2.x)](https://element.eleme.cn/#/zh-CN) 的部分组件，可以选择[完整引入或按需引入 Element UI](https://element.eleme.cn/#/zh-CN/component/quickstart)。

以下是按需引入时需要在 main.js 中增加的内容：

```javascript
// 使用签到组件、抽奖组件时，需要额外引入 element-ui 的 DatePicker 组件。
import { DatePicker } from 'element-ui';
// 使用抽奖组件时，需要额外引入 element-ui 的 button 样式文件。
import 'element-ui/lib/theme-chalk/button.css';

// 注册组件
Vue.use(DatePicker);
```

### 配置 SDK

请参考[互动功能接收端 SDK](https://help.polyv.net/index.html#/live/js/new_sdk/interactions_launch_sdk/sdk/overview)中的「使用说明」。

在 SDK 初始化完毕后，再加载组件。


## 组件

此处仅提供各功能参考文档链接。

- AnswerCard 答题卡：[参考文档](https://help.polyv.net/index.html#/live/js/new_sdk/interactions_launch_sdk/ui/default/answer_card)
- CheckIn 签到：[参考文档](https://help.polyv.net/index.html#/live/js/new_sdk/interactions_launch_sdk/ui/default/check_in)
- Timer 计时器：[参考文档](https://help.polyv.net/index.html#/live/js/new_sdk/interactions_launch_sdk/ui/default/timer)
- Lottery 抽奖：[参考文档](https://help.polyv.net/index.html#/live/js/new_sdk/interactions_launch_sdk/ui/default/lottery)

## 版本更新

暂无
