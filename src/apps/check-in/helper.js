/**
 * 计算签到率
 * @param {Number} num1 被除数
 * @param {Number} num2 除数
 * @returns {String}
 */
export function calculatePercent(num1, num2) {
  num1 = parseInt(num1, 10);
  num2 = parseInt(num2, 10);
  if (!num1 || !num2) return '0%';
  // LIVE-29251 虚拟人数增加725人后，发起签到，签到后云课堂统计人数签到率为0
  // return parseInt(num1 / num2 * 10000, 10) / 100 + '%';
  return parseFloat(num1 * 100 / num2).toFixed(2).replace('.00', '') + '%';
}
