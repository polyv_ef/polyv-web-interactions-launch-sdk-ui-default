# EnrollLottery 抽奖


## 引入组件

```js
import EnrollLottery from '@polyv/interactions-launch-sdk-ui-default/lib/EnrollLottery';
```



## 代码示例

```vue
<template>
  <!-- 建议弹窗宽度为 560px，弹窗内容高度建议自适应，最高height: 500px -->
  <el-dialog
    v-if="checkInCore"
    :custom-class="'c-interactions-dialog c-interactions-dialog__h500'"
    width="560px"
    title="报名抽奖"
    :visible.sync="visible"
    :close-on-click-modal="false"
  >
    <EnrollLottery
      :lang="lang"
      :core="enrollLotteryCore"
    />
  </el-dialog>
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import EnrollLottery from '@polyv/interactions-launch-sdk-ui-default/lib/EnrollLottery';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    EnrollLottery,
  },
  data() {
    return {
      enrollLotteryCore: null,
      visible: false,
      liveStatus: true
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.enrollLotteryCore = new InteractionsLaunchSDK.EnrollLottery();
  }
};
</script>
```



## Attributes

| 属性名     | 类型          | 默认值  | 含义                                                         |
| ---------- | ------------- | ------- | ------------------------------------------------------------ |
| core       | object        |         | 抽奖 SDK 实例                                              |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |
| isSubsidiaryRoom |  boolean   |    false   | 是否开启多房间。为 true 时关闭预设功能。 |
| maxWinnersDigit |  number   |    3   | 最大中奖人数位数                              |
