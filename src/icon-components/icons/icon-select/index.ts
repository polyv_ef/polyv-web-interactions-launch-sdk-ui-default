/**
 * @file IconSelect Icon
 * @author Auto Generated by @polyv/icons-cli
 */

import { IconBuilder } from '@polyv/icons-vue/icon-builder';

export default IconBuilder(
  'icon-select',
  (data) => `
<svg width="${data.size}" height="${data.size}" viewBox="0 0 48 48">
  <g
    fill="none"
    fill-rule="evenodd"
    stroke="none"
    stroke-linecap="${data.strokeLinecap}"
    stroke-width="${data.strokeWidth * 0.75}"
  >
    <g
      stroke="#366BEE"
      stroke-width="${data.strokeWidth * 1.5}"
      transform="translate(-393,-177)"
    >
      <g transform="translate(24,141)">
        <g transform="translate(369,35.88)">
          <path d="M9 24 39 24M24 9 24 39" />
        </g>
      </g>
    </g>
  </g>
</svg>
`
);
