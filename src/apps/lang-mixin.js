import locale from '@/assets/locale';
import en from '@/assets/locale/lang/en';
import zhCN from '@/assets/locale/lang/zh-CN';
import Locale from '@/mixins/locale';

function commonChangeLang(localeName) {
  const langs = {
    en,
    zh_CN: zhCN,
  };
  const defaultLocale = zhCN;
  locale.use(langs[localeName] || defaultLocale);
}

export default {
  mixins: [Locale],

  inject: ['_interactionApp'],

  computed: {
    interactionAppLang() {
      return this._interactionApp.lang;
    },
  },

  watch: {
    interactionAppLang: {
      immediate: true,
      handler(lang) {
        this.updateLangData(lang);
      }
    },
  },
  methods: {
    updateLangData(lang) {
      commonChangeLang(lang);
      this.$forceUpdate();
    },
  }
};
