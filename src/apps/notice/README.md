# Notice 公告

## 引入组件

```js
import Notice from '@polyv/interactions-launch-sdk-ui-default/lib/Notice';
```



## 代码示例

```vue
<template>
	<div>
    <button @click="noticeVisible = true">公告</button>
    <!-- 建议弹窗宽度为 560px，弹窗内容高度为 350px -->
    <el-dialog
      v-if="noticeCore"
      class="c-interactions-lottery"
      :custom-class="'c-interactions-dialog c-interactions-dialog__h350'"
      width="560px"
      title="公告"
      :visible.sync="noticeVisible"
      :close-on-click-modal="false"
    >
      <notice
        :lang="lang"
        :core="noticeCore"
      />
    </el-dialog>
  </div>
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import Notice from '@polyv/interactions-launch-sdk-ui-default/lib/Notice';

export default {
  components: {
    Notice,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',
      noticeVisible: false,
      // 公告 SDK 实例
      noticeCore: null,
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.noticeCore = new InteractionsLaunchSDK.Notice();
  },
};
</script>
```

## Attributes

| 属性名  | 类型             | 默认值  | 含义                                                         |
| ------- | ---------------- | ------- | ------------------------------------------------------------ |
| core    | Object           | null    | 公告 SDK 实例                                                |
| lang    | 'zh_CN'\|'en'    | 'zh_CN' | 语言包类型                                                   |
| isSimpleMode | Boolean | false   | 是否为简单模式。默认情况下，公告组件包含功能：编辑发布公告、移除公告、显示房间内最新公告。为简单模式时，只包含编辑发布公告功能。其它功能需要自行调用接口进行处理。 |


## Events

| 事件名 | 说明                                                         | 回调参数                                                     |
| ------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| close  | 如果属性 isSimpleMode 为 true，可以在该事件回调中隐藏公告组件。 | {type:'published'\|'cancelPublish'}<br />分别表示触发事件的时机为“收到新公告”和“取消发布公告”。 |
