import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Main from './Main.vue';
// 使用签到组件、抽奖组件时，需要额外引入 element-ui 的 DatePicker 组件。
import { DatePicker, Tooltip, Message, TimePicker } from 'element-ui';

// 使用抽奖组件时，需要额外引入 element-ui 的 button 样式文件。
import 'element-ui/lib/theme-chalk/button.css';
import './assets/styles/normalize.css';

Vue.config.productionTip = false;

// 注册组件
Vue.use(DatePicker);
Vue.use(TimePicker);
Vue.prototype.$message = Message;
Vue.use(Tooltip);

Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: 'zh_CN',
  messages: {
    zh_CN: {
      lang: '语言',
      langEnglish: '英文-English',
      langChinese: '中文-Chinese',
      timerCloseTips: '关闭后将停止计时，确认关闭？',
    },
    en: {
      lang: 'language',
      langEnglish: 'English-英文',
      langChinese: 'Chinese-中文',
      timerCloseTips: 'Timer will turn off',
    }
  },
});

const MainComp = Vue.extend(Main);

new MainComp({
  el: '#app',
  i18n,
});
