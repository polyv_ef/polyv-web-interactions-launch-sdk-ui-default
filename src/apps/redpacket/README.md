# Lottery 红包


## 引入组件

```js
import redpacket from '@polyv/interactions-launch-sdk-ui-default/lib/redpacket';
```



## 代码示例

```vue
<template>
  <!-- 建议弹窗宽度为 560px，弹窗内容高度建议自适应 -->
  <el-dialog
    v-if="redpacketCore"
    :custom-class="'c-interactions-dialog c-interactions-dialog__h350'"
    width="560px"
    title="红包"
    :visible.sync="redpacketVisible"
    :close-on-click-modal="false"
  >
    <redpacket
      :lang="lang"
      :core="redpacketCore"
      :redpacket-config="redpacketConfig"
    />
  </el-dialog>
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import redpacket from '@polyv/interactions-launch-sdk-ui-default/lib/redpacket';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    redpacket,
  },
  data() {
    return {
      redpacketCore: null,
      liveStatus: true,
      /** 红包开关配置 */
      redpacketConfig: ['rain', 'normal', 'password']
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.redpacketCore = new InteractionsLaunchSDK.redpacket();
  }
};
</script>
```



## Attributes

| 属性名     | 类型          | 默认值  | 含义                                                         |
| ---------- | ------------- | ------- | ------------------------------------------------------------ |
| core       | object        |         | 抽奖 SDK 实例                                              |
| redpacketConfig | Array        |  []       |  开启对应红包类型功能 normal：普通红包；password：口令红包；rain：红包雨   |
| redpackPresetEnabled | Boolean    |  false    |  是否开启预设红包功能                                 |
| scoreRedPackEnabled | Boolean    |  false    |  是否开启积分红包功能                                 |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |

