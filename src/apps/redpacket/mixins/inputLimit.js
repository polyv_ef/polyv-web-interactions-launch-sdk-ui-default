export default {
  props: {
    /** 拼手气红包总金额上限 */
    randomMaxTotalAmount: {
      type: Number,
      default: 9999
    },
    /** 拼手气红包红包数量上限 */
    randomMaxCount: {
      type: Number,
      default: 999
    },
    /** 等分红包单个金额上限 */
    equalSingleMaxAmount: {
      type: Number,
      default: 999999,
    },
    /** 等分红包单个数量上限 */
    equalSingleMaxCount: {
      type: Number,
      default: 999,
    },
    /** 等分红包总金额上限 */
    equalMaxTotalAmount: {
      type: Number,
      default: 19999,
    },
  }
};
