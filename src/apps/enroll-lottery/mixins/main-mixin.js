/**
 * @file 报名抽奖
 */
import { set } from '@polyv/utils/src/cookie.js';

export default {
  data() {
    return {
      lotteryProcess: 'create',
      currentMenu: 'create',
      lotterying: true,
      searchList: [], // 查询用户列表
      historyPrize: [],
      amount: '', // 中奖人数
      prize: '', // 奖品名称
      totalAccount: 0, // 可中奖列表人数
      lotteryId: '',
      page: 1, // 查询中奖名单页数
      presetList: [], // 预设中奖名单
      currentWinnerList: [], // 开奖后显示的中奖名单
      currentWinnerstotalPages: null,
      allowRequest: true, // 是否可以请求获取列表接口，避免重复请求相同内容
      collectInfo: [
        {
          field: this.t('plvIA.lottery.address'),
          tips: this.t('plvIA.lottery.enterAddress')
        }
      ], // 自定义用户信息
      noOnlineUsers: false, // 无人中奖是因为没有观众在线
      scrollPage: 2,
      drawClock: null,
      lotteryRecordListData: { // 抽奖记录数据
        totalItems: 0,
        contents: []
      },
    };
  },

  props: {
    core: {
      type: Object,
      default: null,
    },
  },

  watch: {
    async lotteryProcess(val) {
      // 当界面为等待抽奖，且未截止报名，则每5秒请求一次报名名单接口，获取报名数
      this.clearClock(this.refreshClock);
      if (val === 'wait' && this.countdownTime > 0) {
        this.updateEnrollNum();
      }
      // 当从等待开奖界面或从抽奖记录-再抽一次界面进入进入设置界面时，判断本地名单是否存在
      if (val === 'wait-edit' || val === 'record-edit') {
        const result = await this.getEnrollNameList(1, 'local');
        this.hasLocalNameList = result && result.totalItems !== 0;
        console.info(result, this.hasLocalNameList);
      }
    }
  },

  async created() {
    // if (this.lotteryProcess !== 'create') return;
    this.getOnLottery();
    this.bindEvents();
  },

  destroyed() {
    this.clearClock(this.drawClock);
    this.clearClock(this.countdownClock);
    this.clearClock(this.refreshClock);
    this.unbindEvents();
  },

  methods: {
    bindEvents() {
      this.core
        // .on('updateLotteryDetail', () => {})
        .on('startLotteryForSignUp', this.handleStartLotteryForSignUp)
        .on('endLotteryForSignUp', this.handleEndLotteryForSignUp);
    },
    unbindEvents() {
      this.core
        // .off('updateLotteryDetail', () => {})
        .off('startLotteryForSignUp', this.handleStartLotteryForSignUp)
        .off('endLotteryForSignUp', this.handleEndLotteryForSignUp);
    },
    handleStartLotteryForSignUp() {
      // 开启报名模式，监听开始抽奖事件展示动画
      if (this.detailData?.sourceRange.includes('enroll')) {
        this.setNextPaths('lotterying');
        this.lotterying = true;
      }
    },
    handleEndLotteryForSignUp(msg) {
      // 没启报名模式，监听结束抽奖事件展示动画
      if (!this.detailData?.sourceRange.includes('enroll')) {
        this.setNextPaths('lotterying');
        this.lotterying = true;
      }
      setTimeout(() => {
        if (!this.detailData.lotteryId) {
          this.setNextPaths('lotterying');
          this.lotterying = true;
        }
        // 重置中奖相关数据
        this.scrollPage = 2;
        this.currentWinnerList = [];
        this.lotteryId = msg.lotteryId;
        this.lotteryTimes = msg.times;
        this.lotteryName = msg.name;
        this.getWinnerList({ lotteryId: this.lotteryId, pageSize: 30, times: this.lotteryTimes });
      }, 3000);
    },
    clearClock(clock) {
      clearTimeout(clock);
      clock = null;
    },
    // 获取抽奖中数据, 并初始化定时器
    async getOnLottery() {
      const result = await this.getLotteryList({ type: 'normal' });
      if (result) {
        const latestItem = result.contents[0];
        if (latestItem) {
          this.setNextPaths('wait');
          this.initDetailData(latestItem);
          this.countdown();
        }
      }
    },
    // 重发抽奖返回
    reStartLotteryBack() {
      this.lotteryProcess = 'create';
      this.handleMenuChange('record');
      const defaultDetailData = {
        name: '',
        sourceRange: 'enroll',
        file: null,
        startTime: Date.now(),
        endTime: 0,
        lotteryType: 'manual',
        amount: '',
        filterWinner: 'N',
        times: 0,
        lotteryId: ''
      };
      this.hasLocalNameList = false;
      this.initDetailData(defaultDetailData);
    },

    // 切换菜单-新建抽奖/抽奖记录
    async handleMenuChange(menu) {
      this.currentMenu = menu;
      if (this.currentMenu === 'record') {
        this.lotteryRecordListPage = 1;
        const result = await this.getLotteryList();
        if (result) {
          this.lotteryRecordListData.contents = result.contents;
          this.lotteryRecordListData.totalItems = result.totalItems;
        }
      }
    },

    newTurnLottery() {
      this.setNextPaths('record-edit');
    },

    // 发起抽奖
    async saveLotteryInfo(options) {
      // options.channelId = Number(this.roomId);
      options.sessionId = this.newSessionId || this.sessionId;
      this.options = options;
      try {
        await this.createLottery(this.options);
      } catch (error) {
        this.$iaMessage.default(error.message);
      }
    },

    async createLottery(options) {
      this.launching = true;
      try {
        await this.core.saveInfo(options);
        this.$iaMessage.default('保存成功');
        this.detailData.sourceRange = options.sourceRange;
        await this.getOnLottery();
      } catch (e) {
        this.$iaMessage.default(e.message);
      }
      this.launching = false;
    },

    setCookie() {
      if (this.historyPrize.some(item => item === this.prize)) return;
      this.historyPrize.unshift(this.prize);
      if (this.historyPrize.length >= 10) this.historyPrize = this.historyPrize.slice(0, 10);
      set('prizeHis', this.historyPrize);
    },

    // 继续抽奖
    async onContinue() {
      this.setNextPaths('record-edit');
      this.launching = false;
      const res = await this.getLotteryList();
      res.contents[0].startTime = null;
      res.contents[0].endTime = null;
      this.initDetailData(res.contents[0]);
    },

    // 开奖-中奖列表-滚动到底部
    async onScrollBottom() {
      // 上一个请求还未返回，或者全部数据已请求，则不再继续请求
      if (!this.allowRequest || this.scrollPage > this.currentWinnerstotalPages) return;
      this.allowRequest = false;
      try {
        await this.getWinnerList({
          lotteryId: this.lotteryId,
          pageNumber: this.scrollPage,
          pageSize: 30,
          times: this.lotteryTimes,
        });
        this.allowRequest = true;
        this.scrollPage += 1;
      } catch (e) {
        this.allowRequest = true;
      }
    },

    // 确认弹窗
    toggleExceedConfirm(toShow) {
      if (toShow) {
        this.$refs.exceedConfirm.show();
      } else {
        this.$refs.exceedConfirm.hide();
      }
    },

    // 确认弹窗-确认
    handleEnsureExceedConfirm() {
      this.toggleExceedConfirm(false);
      if (this.confirmType === 'delete') {
        this.deleteRecord();
      } else if (this.confirmType === 'stop') {
        this.stopLottery();
      }
    },

    // 确认弹窗-取消
    handleCancelExceedConfirm() {
      this.toggleExceedConfirm(false);
      if (this.confirmType === 'delete') {
        this.delLotteryId = '';
      }
    },

    // 初始化抽奖信息
    initDetailData(data) {
      if (!data) return;
      this.detailData = Object.assign({}, data);
      this.lotteryId = data.lotteryId;
      this.newSessionId = data.sessionId;
      this.enrollNum = data.enrollNum;
      this.countdownTime = data.endTime ? (data.endTime - data.serverTime) / 1000 : 0;
      this.lotteryTimes = data.times;
    },

    // 获取抽奖记录
    // eslint-disable-next-line no-unused-vars
    async getLotteryList({ pageNumber = 1, type } = {}) {
      const params = {
        pageNumber,
        lotteryStatus: type,
        pageSize: 4
      };
      return await this.core.getLotteryList(params);
    },

    // TODO:倒计时
    countdown() {
      this.clearClock(this.countdownClock);
      if (this.countdownTime > 0) {
        this.countdownClock = setTimeout(() => {
          this.countdownTime -= 1;
          this.countdown();
        }, 1000);
      } else {
        this.clearClock(this.refreshClock);
      }
    },

    // 设置下一个状态
    setNextPaths(path) {
      if (path) {
        this.paths.push(path);
        this.lotteryProcess = path;
      } else {
        // 当path不存在，则回到上一个状态
        this.paths.pop();
        this.lotteryProcess = this.paths[this.paths.length - 1];
      }
    },

    // 待抽奖-设置抽奖信息-进入抽奖信息详情界面
    async handleSetting() {
      this.setNextPaths('wait-edit');
    },

    // 待抽奖-设置抽奖信息-取消设置-回到等待开奖界面
    cancelEditDetail() {
      this.setNextPaths();
    },

    // 待抽奖-立即开奖
    async handleDrawPrizeNow() {
      if (this.launchDrawPrize) return;
      this.launchDrawPrize = true;
      // 10秒之后若还是未收到发起抽奖的消息，则重置为false
      this.drawClock = setTimeout(() => {
        this.clearClock(this.drawClock);
        if (this.launchDrawPrize) {
          this.launchDrawPrize = false;
        }
      }, 10000);
      this.clearClock(this.countdownClock);
      this.clearClock(this.refreshClock);
      const params = {
        channelId: this.roomId,
        lotteryId: this.lotteryId,
      };
      try {
        await this.core.startLottery(params);
        this.launchDrawPrize = false;
      } catch (e) {
        this.launchDrawPrize = false;
        this.$iaMessage.default(e.message);
      }
    },
    // 待抽奖-终止抽奖-显示确认弹窗
    handleDrawPrizeStop() {
      this.confirmType = 'stop';
      this.toggleExceedConfirm(true);
    },
    // 待抽奖-终止抽奖-终止抽奖
    async stopLottery() {
      this.clearClock(this.countdownClock);
      this.clearClock(this.refreshClock);
      try {
        await this.core.cancelLottery({ lotteryId: this.lotteryId });
        this.setNextPaths('create');
        this.detailData = {
          name: '',
          sourceRange: 'enroll',
          file: null,
          startTime: Date.now(),
          endTime: 0,
          lotteryType: 'manual',
          amount: '',
          filterWinner: 'N',
          times: 0,
          lotteryId: ''
        };
        this.$iaMessage.default('取消成功');
        this.launching = false;
      } catch (e) {
        this.$iaMessage.default(e.message);
      }
    },
    // 获取报名抽奖名单
    async getEnrollNameList(pageNumber = 1, source = 'enroll') {
      const params = {
        lotteryId: this.lotteryId,
        pageNumber,
        pageSize: 4,
        source
      };
      try {
        const res = await this.core.getRecordList(params);
        if (res) {
          this.enrollNameListData.contents = res.contents;
          this.enrollNameListData.totalItems = res.totalItems;
        }
        return res;
      } catch (e) {
        this.$iaMessage.default(e.message);
      }
    },
    // 查看本地/报名名单
    seeNameList(type) {
      this.currentNameListType = type;
      this.getEnrollNameList(1, type);
      this.setNextPaths('name-list');
    },
    onNameListPageChange(v) {
      this.getEnrollNameList(v, this.currentNameListType);
    },
    // 关闭本地/报名名单界面
    onNameListClose() {
      // TODO: 记住最新的修改
      this.setNextPaths();
    },
    // 获取中奖名单
    async getWinnerList({ lotteryId, pageNumber = 1, pageSize = 4, times = 0 } = {}) {
      const params = {
        lotteryId,
        pageNumber,
        pageSize,
      };
      if (times) {
        params.times = times;
      }
      const res = await this.core.getWinnerList(params);
      if (res) {
        const { contents, totalItems } = res;
        // 抽奖记录-查看中奖名单
        if (this.lotteryProcess === 'create') {
          this.winnerListData.totalItems = totalItems;
          this.winnerListData.contents = contents;
        } else {
          // 立即开奖-中奖名单
          this.currentWinnerList = this.currentWinnerList.concat(contents);
          this.currentWinnerstotalPages = res.totalPages;
          this.lotterying = false;
        }
      }
    },
    // 中奖记录-翻页
    async handleLotteryRecordsPageChange(page) {
      this.lotteryRecordListPage = page;
      const res = await this.getLotteryList({ pageNumber: page });
      if (res) {
        this.lotteryRecordListData.contents = res.contents;
      }
    },
    // 抽奖记录-再次抽奖
    handleAgainLottery(data) {
      // 复用抽奖置空时间
      data.startTime = data.endTime = null;
      this.initDetailData(data);
      this.setNextPaths('record-edit');
      this.currentMenu = 'create';
      this.launching = false;
    },
    // 抽奖记录-获取中奖名单
    handleGetWinners(item) {
      this.showWinnerList = true;
      this.winnersLotteryId = item.lotteryId;
      this.currentLotteryName = item.name;
      this.getWinnerList({ lotteryId: item.lotteryId });
    },
    // 抽奖记录-获取中奖名单-翻页
    onWinnerListPageChange(page) {
      this.getWinnerList({ lotteryId: this.winnersLotteryId, pageNumber: page });
    },
    // 中奖记录-中奖名单列表-返回
    onWinnerListClose() {
      this.showWinnerList = false;
      this.winnersLotteryId = '';
      this.currentLotteryName = '';
    },
    // 抽奖记录-删除记录-显示确认弹窗
    handleDeleteRecord(lotteryId) {
      this.delLotteryId = lotteryId;
      this.confirmType = 'delete';
      this.toggleExceedConfirm(true);
    },
    // 抽奖记录-删除记录
    async deleteRecord() {
      const params = {
        lotteryId: this.delLotteryId
      };
      try {
        await this.core.deleteRecord(params);
        const listRes = await this.getLotteryList();
        if (listRes) {
          this.lotteryRecordListData.contents = listRes.contents;
          this.lotteryRecordListData.totalItems = listRes.totalItems;
        }
      } catch (e) {
        throw new Error(e);
      }
    },
    // 轮询抽奖记录接口，获取报名人数，更新倒计时
    updateEnrollNum() {
      this.refreshClock = setInterval(async () => {
        const result = await this.getLotteryList({ type: 'normal', isInit: false });
        if (result) {
          const latestItem = result.contents[0];
          if (latestItem) {
            this.enrollNum = latestItem.enrollNum;
            this.countdownTime = latestItem.endTime ? (latestItem.endTime - latestItem.serverTime) / 1000 : 0;
          }
        }
      }, 5000);
    }
  }
};
