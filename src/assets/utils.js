const hasOwnProperty = Object.prototype.hasOwnProperty;

export function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

/**
 * 将接收到的公告内容转为点击链接可复制内容的 HTML 字符串。
 * @param noticeContent - 公告内容。
 * @returns 点击链接可复制内容的 HTML 字符串。
 */
export function toSpecialHtml(noticeContent) {
  // 匹配 a 标签的正则
  const aReg = /<a\shref="(.+?)"\starget="_blank"\srel="noreferrer\snoopener">(.+?)<\/a>/g;
  return noticeContent.replace(aReg, '<button class="plv-ia-clipboard-btn" data-clipboard-text="$1">$2</button>');
}

/**
 * 复制字符串内容。
 * @param {String} pasteContent - 需要复制到粘贴板的内容。
 * @param {Function} callback - 复制成功后的回调。
 * @returns 一个 Boolean ，如果是 false 则表示操作不被支持或未被启用。
 */
export function paste(pasteContent, callback) {
  const textarea = document.createElement('textarea');
  textarea.textContent = pasteContent;
  textarea.style.position = 'fixed'; // Prevent scrolling to bottom of page in MS Edge.
  document.body.appendChild(textarea);
  textarea.select();
  try {
    return document.execCommand('copy'); // Security exception may be thrown by some browsers.
  } catch (ex) {
    console.warn('Copy to clipboard failed.', ex);
    return false;
  } finally {
    document.body.removeChild(textarea);
    callback && callback();
  }
}

/**
 * 加载线上 js 资源。
 * @param {String} src - js 资源地址。
 * @param {Function} cb - 加载成功时的回调。
 * @param {String} [name] - js 资源加载成功后会挂载到 window 变量上的名字。如果已经存在，就不需要重复加载。
 */
export function loadScript(src, cb, name) {
  if (name && name in window) {
    if (typeof cb === 'function') cb.call(window);
    return;
  }

  const script = document.createElement('script');
  script.setAttribute('src', src);
  document.body.appendChild(script);
  script.onload = () => {
    if (typeof cb === 'function') cb.call(window);
    document.body.removeChild(script);
  };
}

/**
 * 防抖
 * @param {Function} method 回调函数
 * @param {number} delay 延迟时间执行
 * */

export function debounce(method, delay = 500) {
  let timer = null;
  return function(...args) {
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      method.call(this, ...args);
    }, delay);
  };
}
