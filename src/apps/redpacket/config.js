// 拼手气红包金额最大值
export const RANDOM_MAX_ACCOUNT = 9999;
// 拼手气红包均分金额最小值
export const SINGLE_MIN_ACCOUNT = 0.3;
// 拼手气红包均分积分最小值
export const SINGLE_MIN_SCORE = 1;
// 拼手气红包金额最小值
export const RANDOM_MIN_TOTAL_ACCOUNT = 1;
// 拼手气红包积分最小值
export const RANDOM_MIN_TOTAL_SCORE = 1;
// 拼手气红包数量最大值
export const RANDOM_MAX_COUNT = 999;

// 等分红包总金额最小值
export const EQUAL_MIN_TOTAL_ACCOUNT = 1;
// 等分红包总积分最小值
export const EQUAL_MIN_TOTAL_SCORE = 1;
// 等分红包总金额最大值
export const EQUAL_MAX_TOTAL_ACCOUNT = 19999;
// 等分红包各红包金额最大值
export const EQUAL_SIGNLE_MAX_ACCOUNT = 999999;
// 等分红包各红包数量最大值
export const EQUAL_SIGNLE_MAX_COUNT = 999;

// 支付宝口令红包总金额最小值
export const ALIPAY_PWD_MIN_TOTAL_ACCOUNT = 0.01;
// 支付宝口令红包总金额最大值
export const ALIPAY_PWD_MAX_TOTAL_ACCOUNT = 10000;
// 支付宝口令红包数量最大值
export const ALIPAY_PWD_MAX_COUNT = 10000;
// 支付宝口令红包数量最小值
export const ALIPAY_PWD_MIN_COUNT = 1;

// 默认红包参数
export const DEFAULT_DATA = {
  curAccount: '', // 红包金额 拼手气 [1, 9999]
  equalData: [
    {
      account: '',
      count: ''
    }
  ], // 等分起红包数据
  rpCount: '', // 拼手气红包个数 [1, 999]
  // greeting: '恭喜发财，大吉大利', // 封面文字 0-20
  joinInOption: 'none', // 参与条件
  passwd: '', // 口令 0-20个字符
  passwdTip: '' // 口令提示
  // sendDisabled: true
};
// 默认红包金额 中文
export const DEFAULT_ACCOUNTSCN = [
  { value: 1.88, label: '1.88元' },
  { value: 18.88, label: '18.88元' },
  { value: 188.88, label: '188.88元' },
  { value: 1888.88, label: '1888.88元' }
];

// 默认红包金额 英文
export const DEFAULT_ACCOUNTSEN = [
  { value: 1.88, label: '￥1.88' },
  { value: 18.88, label: '￥18.88' },
  { value: 188.88, label: '￥188.88' },
  { value: 1888.88, label: '￥1888.88' }
];

// 默认积分红包金额 中文
export const DEFAULT_POINT_ACCOUNTSCN = [
  { value: 1, label: '1个' },
  { value: 10, label: '10个' },
  { value: 100, label: '100个' },
  { value: 1000, label: '1000个' },
];

// 默认积分红包金额 英文
export const DEFAULT_POINT_ACCOUNTSEN = [
  { value: 1, label: '1' },
  { value: 10, label: '10' },
  { value: 100, label: '100' },
  { value: 1000, label: '1000' },
];

// 倒计时最大值和最小值
export const DEFAULT_DELAY_LIMIT = {
  max: 99, // 最大99分钟
  min: 1 // 最小1分钟
};

// 红包页面
export const step = {
  // 预设红包页面
  preRedpacket: 'preRedpacket',
  // 发红包成功
  sendRedpacketSuccess: 'sendRedpacketSuccess',
  // 创建红包页
  createRedpacket: 'createRedpacket',
  // 预设红包详情
  preRedpacketDetail: 'preRedpacketDetail',
};
