export default class Timer {
  constructor({
    secs,
    mode = 'sub',
    limit = 0,
    cb = () => {}
  }) {
    this.cb = cb;
    this.limit = limit;
    this._mode = mode;
    this._secs = this.secs = secs;

    this._stopped = true;
  }

  get _isAddMode() {
    return this._mode === 'add';
  }

  get _limit() {
    const { limit, secs } = this;
    return this._isAddMode ? (limit > secs ? limit : 60 * 60) : (limit <= 0 ? 0 : limit);
  }

  _exec() {
    const time = 1000 - (Date.now() - this._startTime);
    setTimeout(() => {
      if (this._stopped) return;
      if (this._isAddMode) {
        this._secs += 1;
        if (this._secs > this._limit) {
          this.stop();
        } else if (typeof this.cb === 'function') {
          this.cb();
        }
      } else {
        this._secs -= 1;
        if (this._secs < this._limit) {
          this.stop();
        } else if (typeof this.cb === 'function') {
          this.cb();
        }
      }
      this._startTime = Date.now();
      this._exec();
    }, time > 0 ? time : 0);
  }

  start() {
    this._startTime = Date.now();
    this._stopped = false;
    this._exec();
  }

  stop() {
    this._stopped = true;
  }
}
