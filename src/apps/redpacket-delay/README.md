# redpacket 倒计时红包


## 引入组件

```js
import redpacket from '@polyv/interactions-launch-sdk-ui-default/lib/redpacket';
```



## 代码示例

```vue
<template>
  <!-- 建议弹窗宽度为 560px，弹窗内容高度建议自适应 -->
  <el-dialog
    v-if="redpacketCore"
    :custom-class="'c-interactions-dialog c-interactions-dialog__h350'"
    width="560px"
    title="红包"
    :visible.sync="redpacketVisible"
    :close-on-click-modal="false"
  >
    <redpacket-delay
      :core="redpacketCore"
    />
  </el-dialog>
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import RedpacketDelay from '@/apps/redpacket-delay/RedpacketDelay';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    RedpacketDelay,
  },
  data() {
    return {
      redpacketCore: null,
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.redpacketCore = new InteractionsLaunchSDK.redpacket();
  }
};
</script>
```



## Attributes

| 属性名     | 类型          | 默认值  | 含义                                                         |
| ---------- | ------------- | ------- | ------------------------------------------------------------ |
| core       | object        |         | 抽奖 SDK 实例                                              |
| delayTime       | number        |    0     |  倒计时时间（毫秒）, 传入delayTime 不再从接口获取倒计时       |
