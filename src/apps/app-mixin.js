import Vue from 'vue';
import Message from '@/components/message';
import locale from '@/assets/locale';
import en from '@/assets/locale/lang/en';
import zhCN from '@/assets/locale/lang/zh-CN';
import soundEffects from '@/mixins/sound-effects/sound-effects';

function commonChangeLang(localeName) {
  const langs = {
    en,
    zh_CN: zhCN,
  };
  const defaultLocale = zhCN;
  locale.use(langs[localeName] || defaultLocale);
  document.body.setAttribute('data-lang', localeName);
}

if (!window.__isloggerVer2ILSUIDef) {
  window.__isloggerVer2ILSUIDef = true;
  console.info(`[interactions-launch-sdk-ui-default][version] ${APP_VERSION} / ${APP_BUILD_TIME}`);
}

export default {
  mixins: [soundEffects],

  provide() {
    return {
      _interactionApp: this,
    };
  },

  props: {
    /** 语言包类型 */
    lang: {
      type: String,
      default: 'zh_CN',
      validator(val) {
        return ['en', 'zh_CN'].includes(val);
      }
    },
    /** 当前是否可以发起互动。 */
    canStart: {
      type: Boolean,
      default: true,
    },

    /** 如果当前不可以发起互动，点击发起按钮时的提示文案。 */
    cantStartTips: {
      type: String,
      default: '当前不可以发起互动',
    },

    /** 研讨会 是否使用研讨会接口 */
    useSeminarChatApi: {
      type: Boolean,
      default: false,
    },
    /** 是否使用互动toast */
    useInteractionsToast: {
      type: Boolean,
      default: true,
    },
    /** 是否开启严禁词校验 */
    checkDefaultBadwordEnabled: {
      type: Boolean,
      default: true,
    },
  },
  data() {
    return {
      isCoreExisted: false,
    };
  },
  watch: {
    core: {
      immediate: true,
      handler(core) {
        if (!core || this.isCoreExisted) return;
        this.isCoreExisted = true;
        // 在保证 core 存在后再去调用 core 的方法
        if (this.afterCoreInited && typeof this.afterCoreInited === 'function') {
          this.afterCoreInited();
        }
      },
    },
    lang: {
      immediate: true,
      handler(lang) {
        commonChangeLang(lang);
        this.$forceUpdate();
      }
    }
  },
  mounted() {
    if (this.$refs.container) {
      if (!Vue.prototype.$iaMessage) {
        Vue.prototype.$iaMessage = {};
      }
      Vue.prototype.$iaMessage.default = (options) => {
        if (this.useInteractionsToast) {
          Message(this.$refs.container)(options);
        } else {
          this.$emit('alert-message', options);
        }
      };
    }
  },
  methods: {
    async checkDefaultBadword(content) {
      if (!this.checkDefaultBadwordEnabled || !content) return;
      let badwords;
      try {
        const res = await this.core.checkDefaultBadword(content);
        badwords = res.status ? undefined : res.badwords.join('、');
      } catch (error) {
        console.warn(error);
      }
      return badwords;
    },
  },
};
