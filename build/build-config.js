/**
 * @file 包信息
 */

const path = require('path');
const pkgJSON = require('../package');

// 开发服务器局域网ip
exports.devServerHost = '0.0.0.0';

// 构建环境
exports.env = process.env.BUILD_ENV;

// UMD 模块占用的全局变量名
exports.libGlobal = 'PolyvILScene';

// OSS 上的路径
exports.ossPath = '/interactions-launch-sdk-ui-default/';

// 发布后的正式版本
exports.version = pkgJSON.version;

// 目标目录
exports.currentLibDest = path.resolve(__dirname, '../dist/');

// 需要编译的依赖
exports.transpileDependencies = [
  '@just4',
  'lodash-es',
  '@polyv/utils',
  { name: '@polyv/polyv-ui', css: true, assets: true },
  { name: 'element-ui', css: true, assets: true }
];

// 开发服务器端口号
exports.devServerPort = 23003;

// 本地开发服务器代理
exports.devServerProxy = (function() {
  const getProxyReqFunction = (host) => {
    return (proxyReq, req) => {
      // 复制本地头部
      Object.keys(req.headers).forEach((key) => {
        proxyReq.setHeader(key, req.headers[key]);
      });
      proxyReq.setHeader('Host', host);
    };
  };

  const liveHost = 'live.polyv.net';

  return {
    '/v2': {
      target: 'https://console.polyv.net',
      changeOrigin: true,
      onProxyReq: getProxyReqFunction(liveHost),
    },
    '/teacher': {
      target: 'http://live.polyv.net',
      changeOrigin: true,
      onProxyReq: getProxyReqFunction(liveHost),
    },
  };
})();

// 别名配置
exports.alias = {
  '@': path.resolve(__dirname, '../src')
};
