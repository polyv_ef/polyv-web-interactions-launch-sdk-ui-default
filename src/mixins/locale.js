import { t } from '@/assets/locale';

export default {
  props: {
    /** 语言包类型 */
    lang: {
      type: String,
      default: 'zh_CN',
      validator(val) {
        return ['en', 'zh_CN'].includes(val);
      }
    },
  },

  methods: {
    t(...args) {
      return t.apply(this, args);
    }
  }
};
