/**
 * 元素外部点击事件
 */
const ClickOutSide = {
  name: 'click-out-side',

  bind(el, binding, vnode) {
    const handleDocumentClick = (e) => {
      const target = e.target;

      // 点击的元素是否在插入body的弹出窗元素
      const isInPopper = (vnode.context.popperElm &&
        (vnode.context.popperElm.contains(target)));

      if (isInPopper || el.contains(target)) return;
      binding.value(e);
    };
    document.addEventListener('mouseup', handleDocumentClick);
  },
};
ClickOutSide.install = function(Vue) {
  Vue.directive(ClickOutSide.name, ClickOutSide);
};

export default ClickOutSide;
