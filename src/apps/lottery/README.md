# Lottery 抽奖


## 引入组件

```js
import Lottery from '@polyv/interactions-launch-sdk-ui-default/lib/Lottery';
```



## 代码示例

```vue
<template>
  <!-- 建议弹窗宽度为 560px，弹窗内容高度建议自适应，最高height: 500px -->
  <el-dialog
    v-if="checkInCore"
    :custom-class="'c-interactions-dialog c-interactions-dialog__h350'"
    width="560px"
    title="签到"
    :visible.sync="checkInVisible"
    :close-on-click-modal="false"
  >
    <Lottery
      :lang="lang"
      :core="lotteryCore"
      :live-status='liveStatus'
      :is-subsidiary-room="false"
    />
  </el-dialog>
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import Lottery from '@polyv/interactions-launch-sdk-ui-default/lib/Lottery';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    Lottery,
  },
  data() {
    return {
      lotteryCore: null,
      liveStatus: true
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.lotteryCore = new InteractionsLaunchSDK.Lottery();
  }
};
</script>
```



## Attributes

| 属性名     | 类型          | 默认值  | 含义                                                         |
| ---------- | ------------- | ------- | ------------------------------------------------------------ |
| core       | object        |         | 抽奖 SDK 实例                                              |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |
| liveStatus | number        | false   | 直播状态。开始直播后才能使用抽奖功能。 |
| isSubsidiaryRoom |  boolean   |    false   | 是否开启多房间。为 true 时关闭预设功能。 |
| maxWinnersDigit |  number   |    3   | 最大中奖人数位数                              |
| visible       | boolean  | true |  是否显示抽奖                                                 |
