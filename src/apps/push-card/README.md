# PushCard 卡片推送

## 引入组件

```js
import PushCard from '@polyv/interactions-launch-sdk-ui-default/lib/PushCard';
```



## 代码示例

```vue
<template>
	<!-- 建议弹窗宽度为 720px，弹窗内容高度为 500px -->
  <el-dialog
    v-if="checkInCore"
    :custom-class="'c-interactions-dialog c-interactions-dialog__h500'"
    width="720px"
    title="卡片推送"
    :visible.sync="visible"
    :close-on-click-modal="false"
  >
    <PushCard
      :lang="lang"
      :core="pushCardCore"
      :right="{
        sendResult: true,
        showHistoryEntry: false,
        download: false,
      }"
    />
  </el-dialog>
</template>

<script>
import PushCard from '@polyv/interactions-launch-sdk-ui-default/lib/PushCard';
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    PushCard,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',
      pushCardCore: null,
      visible: false,
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.PushCardCore = new InteractionsLaunchSDK.PushCard();
  }
};
</script>
```



## Attributes

| 属性名        | 类型          | 默认值                                                     | 含义                                                         |
| ------------- | ------------- | ---------------------------------------------------------- | ------------------------------------------------------------ |
| core          | object        | null                                                       | 卡片推送 SDK 实例                                                |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |
| canStart      | boolean       | true                                                       | 当前是否可以发起互动。                                       |
| cantStartTips | string        | '当前不可以发起互动'                                          | 如果当前不可以发起互动，点击发起按钮时的提示文案。           |
