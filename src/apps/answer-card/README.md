# AnswerCard 答题卡

## 引入组件

```js
import AnswerCard from '@polyv/interactions-launch-sdk-ui-default/lib/AnswerCard';
```



## 代码示例

```vue
<template>
	<!-- 建议弹窗宽度为 800px，弹窗内容高度为 600px -->
  <el-dialog
    :custom-class="'c-interactions-dialog c-interactions-dialog__h600'"
    width="800px"
    title="答题卡"
    :visible.sync="answerCardVisible"
    :close-on-click-modal="false"
  >
    <answer-card
      :lang="lang"
      :core="answerCardCore"
      :result-id="answerCardResultId"
      :channel-setting="{
        defaultLimitTimeAnswer: false,
      }"
    />
  </el-dialog>
</template>

<script>
import AnswerCard from '@polyv/interactions-launch-sdk-ui-default/lib/AnswerCard';
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    AnswerCard,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',

      answerCardCore: null,
      answerCardVisible: false,
      // 需要显示答题卡结果的题目 id
      answerCardResultId: '',
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.answerCardCore = new InteractionsLaunchSDK.AnswerCard();
    this.answerCardCore.on('show-result', ({ questionId }) => {
      this.answerCardVisible = true;
      this.answerCardResultId = questionId;
    });
  }
}
</script>
```



## Attributes

| 属性名         | 类型                                                         | 默认值                                                       | 含义                                                         |
| -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| core           | object                                                       | null                                                         | 答题卡 SDK 实例                                              |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |
| canStart       | boolean                                                      | true                                                         | 当前是否可以发起互动。                                       |
| cantStartTips  | string                                                       | '当前不可以发起互动'                                         | 如果当前不可以发起互动，点击发起按钮时的提示文案。           |
| resultId       | string                                                       | ''                                                           | 需要显示结果的答题卡 id                                      |
| channelSetting | object | { defaultLimitTimeAnswer: false, answerContentBlankEnabled: false,} | 频道自定义设置。类型定义详见[ChannelSetting](#ChannelSetting)。  |
| showLog        | boolean                                                      | false                                                        | 是否显示日志 tab                                             |

### ChannelSetting 对象说明
| 属性名         | 类型                                                         | 默认值                                                       | 含义 |
| -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| defaultLimitTimeAnswer | boolean | false | 设置是否默认启用限时答题，开启该功能时可以设置限时答题卡的时长，倒计时结束后将自动停止答题卡。 |
| answerContentBlankEnabled | boolean | false | 设置答题卡题干和选项内容是否可以为空。 |
