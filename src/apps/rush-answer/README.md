# RushAnswer 抢答

## 引入组件

```js
import RushAnswer from '@polyv/interactions-launch-sdk-ui-default/lib/RushAnswer';
```



## 代码示例

```vue
<template>
	<!-- 建议显示区域宽度为 560px，弹窗内容高度为 350px -->
  <RushAnswer
    :lang="lang"
    :core="core"
  />
</template>

<script>
import RushAnswer from '@polyv/interactions-launch-sdk-ui-default/lib/RushAnswer';
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';

export default {
  components: {
    RushAnswer,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',
      // RushAnswer 实例
      core: null,
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.core = new InteractionsLaunchSDK.RushAnswer();
  }
}
</script>
```



## Attributes

| 属性名 | 类型          | 默认值  | 含义            |
| ------ | ------------- | ------- | --------------- |
| core   | Object        | null    | RushAnswer 实例 |
| lang   | 'zh_CN'\|'en' | 'zh_CN' | 语言包类型      |
