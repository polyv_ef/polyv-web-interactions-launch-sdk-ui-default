
export default {
  plvIA: {
    common: {
      enter: '请输入',
      tips: '提示',
      uploadImg: '上传图片',
      reuploadImg: '重新上传',
      uploadImgTips: '大小不超过 2M，支持 jpg、jpeg、png格式',

      fetchFail: '接口请求失败',
      networkErr: '网络错误，请刷新重试',
      undefinedError: '未知错误',
      failFetchData: '获取数据时出错',

      // tips
      uploadSuccessfullyTips: '上传成功',
      deleteSuccessfullyTips: '删除成功',
      deleteFailedTips: '删除失败',
      sentSuccessfullyTips: '发送成功',
      saveSuccessfullyTips: '保存成功',
      failedToSentTips: '发送失败',
      importSuccessTips: '导入成功',
      copySuccessfullyTips: '复制成功',
      copyUnsuccessfullyTips: '复制失败，该浏览器不支持自动复制',
      editSuccessfullyTips: '修改成功',
      confirmDiscardTips: '上一步操作未保存，点击确认将被丢弃',
      select: '请选择',
      pleaseSearch: '搜索关键词',

      // buttons
      cancel: '取消',
      confirm: '确认',
      save: '保存',
      delete: '删除',
      sent: '已发送',
      exit: '退出',
      back: '返回',

      // label
      privacy: '隐私协议',
      sendText: '发送至',

      operation: '操作',

      checkDefaultBadwordTip1: '内容违规，包含【',
      checkDefaultBadwordTip2: '】请修改',
    },

    datePicker: {
      startTime: '开始日期',
      endTime: '结束日期',
      pickerTxt: '时间筛选'
    },

    answerCard: {
      statusOngoing: '答题中',
      statusTerminated: '已答题',
      confirmDelTips: '删除后将无法恢复，确认删除？',
      confirmCoverTips: '题库名称重复，是否覆盖已有题库？',
      cover: '覆盖',
      confirmDelQuesionTips: '将删除答题卡题目，确认删除？',
      oneChoiceLabel: '单选',
      multipleChoiceLabel: '多选',
      scoreLabel: '评分',
      voteLabel: '投票',
      createQuestion: '新建题目',
      quickQuestion: '快速问答',
      timeLimit: '答题限时',
      answerSheet: '答题卡',
      questionBank: '题库',
      ongoingTips: '正在答题中，请先停止答题',
      defaultBankName: '默认题库',
      bankExistedTips: '该题库已存在，不允许上传',
      bankUsingNoCoverTips: '当前题库正在使用，无法覆盖上传',
      bankName: '名称',
      bankStatus: '状态',
      operation: '操作',
      apply: '应用',
      download: '下载',
      reupload: '重新上传',
      noContent: '暂无更多数据',
      uploadBank: '上传题库',
      downloadTemplate: '下载模板',
      inUsingStatus: '使用中',
      notUsedStatus: '未使用',
      templateErr: '模板错误',
      bankUsingCantDelTips: '该题库正在使用，无法删除',
      defaultCantDelTips: '默认题库，不可删除',
      score: '分',
      person: '人',
      totalSubmission: '共提交',
      correct: '，答对',
      averageScore: '，平均得分',
      stopRating: '停止评分',
      stopAnswer: '停止答题',
      remainingTime: '答题剩余时间',
      submitted: '，已提交',
      submittedStatus: '已提交',
      send: '发送答题卡',
      edit: '编辑',
      sent: '已发送',
      sendResults: '发送结果',
      sendAgain: '再次发题',
      continue: '继续发题',
      enterTimeLimit: '请输入答题时长',
      questionType: '题型：',
      questionTitle: '题目：',
      enterTitle: '请输入题目',
      questionOption: '选项：',
      enterOption: '请输入选项',
      optionScore: '分值',
      enterScorePrompt: '请输入分值提示（非必填）',
      enterQuestionAnswer: '请选择题目的答案',
      enterOptions: '请输入选项内容',
      create: '新建',
      import: '导入',
      optionsCount: '个选项',
      seconds: '秒',
      importFail: '模板导入格式不正确，请重新上传',
      downloadResults: '下载答题结果',
      checkDefaultBadwordTitle: '题目',
      checkDefaultBadwordOption: '选项',
    },

    answerQuickly: {
      ranking: '排名',
      nickname: '昵称',
      institution: '机构',
      responderTimeCost: '抢答用时',
      restart: '再次抢答',
      seconds: '秒',
      secondsShorthand: '秒',
      currently: '当前已有',
      respond: '人抢答',
      endAutomaticallyTips: '倒计时结束后将自动结束抢答',
      endNow: '提前结束',
      responderTime: '抢答倒计时',
      cantEmptyTips: '抢答时间不可为空',
      start: '开始抢答',
      timeTip: '观众需在倒计时结束前完成抢答（可设置3~60秒）',
      responderNumber: '抢答人数',
      people: '人',
      numberTip: '若抢答人数达到设置人数，抢答自动结束（可设置1~20人）',
      remark: '备注',
      remarkPlaceholder: '请输入备注内容',
      timeError: '请输入3~60的数值',
      numberError: '请输入1~20的数值',
      record: '抢答记录',
      nick: '用户昵称',
      time: '抢答时间',
      noAnswerYet: '暂无人抢答',
      end: '结束抢答',
      inviteToMic: '邀请上麦',
      served: '已上麦',
      answerEnd: '抢答结束',
      operate: '操作',
      new: '新建抢答',
      answerOverNoAnswer: '抢答结束，暂无人抢答',
      ranking2: '抢答排名',
      numberOfPeople: '人数',
      isAnswering: '正在抢答',
      countdown: '倒计时',
      answerStartTime: '抢答发起时间',
      numberOfParticipants: '参与人数',
      check: '查看',
      noRecords: '暂无记录',
      userId: '用户id',
      detail: '查看详情',
      loading: '加载中',
    },

    lottery: {
      repeatNoneLotteryTip1: '直播间所有观众均可参与抽奖。',
      repeatNoneLotteryTip2: '关闭后，直播间所有已中奖用户，不参与本轮抽奖。',
      repeatConditionLotteryTip1: '开启后，直播间所有达成抽奖条件的观众，均可参与本轮抽奖。',
      repeatConditionLotteryTip2: '关闭后，直播间所有已中奖用户，不参与本轮抽奖；（注：若某用户已中过奖，在本轮抽奖中，即使达成抽奖条件，也无法中奖）',
      selectLotteryGroup: '请选择分组',
      requiredText: '必填',
      address: '地址：',
      enterAddress: '请输入您的收件地址',
      name: '姓名：',
      userName: '姓名',
      userPhone: '手机号码',
      customOption: '自定义项',
      addCollectOption: '添加采集项',
      enterName: '请输入您的真实姓名',
      phoneNum: '手机：',
      enterPhoneNum: '请输入您的手机号码',
      exceedOnline: '当前在线人数可能少于设置的中奖人数，是否继续抽奖？',
      allOnlineUsers: '全体在线用户',
      missedUsers: '未中奖用户',
      customGroup: '自定义分组',
      congratulationTips: '恭喜以下幸运儿抽中',
      copyList: '复制名单',
      winnersList: '中奖名单',
      noNoneWon: '本轮无人中奖',
      allWon: '参与用户均已中过奖',
      continue: '继续抽奖',
      end: '结束抽奖',
      prizeName: '奖品名称：',
      enterPrizeName: '请输入奖品名称',
      prizeImage: '奖品图片：',
      participatingUsers: '参与用户：',
      selectGroup: '选择分组：',
      winnersNum: '中奖人数：',
      enterPositiveInteger: '请输入正整数',
      presetWinning: '预设中奖：',
      prizeCode: '兑换码：',
      prizeCodeTip: '生成兑奖码，用于核对奖品',
      winnerList: '中奖名单：',
      winnerListTip: '允许观众查看中奖名单',
      deadlineLabel: '中奖信息截止时间：',
      quickSearch: '输入用户昵称可快速搜索',
      moreSettings: '更多设置',
      collection: '中奖信息采集：',
      repeatLotteryTip: '允许重复中奖：',
      addCustomOptions: '添加自定义选项',
      userCollection: '用户填写信息',
      custom: '自定义：',
      title: '标题',
      enterDescription: '请输入您的描述文案',
      start: '开始抽奖',
      exceedWinners: '预设名单超过设置的中奖人数',
      maxGroupTip: '选择分组名单不能超过10个',
      history: '历史记录',
      referenceName: '参考名称',
      grandPrize: '特等奖',
      firstPrize: '一等奖',
      secondPrize: '二等奖',
      thirdPrize: '三等奖',
      startLiveFirst: '请先直播再发起抽奖',
      lotteryMode: '抽奖模式：',
      luckyTime: '抽奖开始时间：',
      luckyTimeEnd: '抽奖结束时间：',
      luckyTimeEndMinute: '分',
      luckyTimeEndSecond: '秒',
      closeLottery: '关闭抽奖',
      immediate: '即时抽奖',
      timed: '定时抽奖（手动结束）',
      timedAuto: '定时抽奖（自动结束）',
      switchOldBranch: '切换至旧版本',
      switchNewBranch: '切换至新版本',
      toast1: '最多设置{t}个',
      minLotteryEndTime: '最小只能输入3秒',
      countDownTimeLabel: 's后自动结束抽奖',
      lotteryTimeThenCurrent: '抽奖时间不能小于当前时间',
      deadlineTip: '观众需要在该时间内填写并提交收货信息',

      statusWaiting: '未开始',
      statusStart: '进行中',
      statusEnd: '已结束',
      statusCancel: '已取消',
      back: '返回',

      createdSweepstakes: '已创建抽奖活动？',
      selectActivity: '选择活动',
      reset: '重置',
      lotterying: '进行中',
      lotteryMethod: '抽奖方式：',
      activityDuration: '活动时长：',
      drawTime: '抽奖时间：',
      numberWinners: '中奖人数：',
      theEnd: '已结束',
      inProgress: '进行中',
      notStarted: '未开始',
      notTemplateList: '暂未创建活动',
      activeOver: '活动已结束',
      viewResults: '查看结果',
      cancelDraw: '取消抽奖',
      questionRecord: '答题记录',
      continueDraw: '新增抽奖',
      endDraw: '立即开奖',
      deadlineTimeLabel: '<span>{time} </span>后自动开奖',
      conditionNone: '无条件',
      conditionDuration: '观看时长',
      conditionInvite: '邀请好友',
      conditionComment: '评论',
      conditionQuestion: '答题',
      lotteryingStatusTip1: '有条件和无条件的抽奖均在进行中，暂时无法发起抽奖',
      lotteryingStatusTip2: '已有条件抽奖活动进行中，只能发起无条件抽奖',
      lotteryingStatusTip3: '已有无条件抽奖活动进行中，只能发起有条件抽奖',
      templateLotteryName: '抽奖名称',
      templatePrizeName: '奖品名称',
      templateDuration: '活动时长',
      templateCondition: '抽奖条件',
      templateAmount: '中奖人数',
      templateHandle: '操作',
      templateChoose: '选择',
      noRecord: '暂无创建活动',
      noWinner: '暂无中奖名单',

      privacyTips: '开启后，观众要同意您设置的隐私声明，才可确认领奖。',
      pleaseEntryCollect: '请添加要收集的信息',

      pushQnText1: '推送第',
      pushQnText2: '题',
      pushQnText3: '共',
      pushQnText4: '题',

      pushQnBtn1: '继续抽奖',
      pushQnBtn2: '结束答题并抽奖',

      pushQnPopperTitle1: '本轮答题将提前结束，请选择奖项',
      pushQnPopperTitle2: '请选择继续抽奖的奖项',

      customGroupLotteryTypeRandom: '随机中奖',
      customGroupLotteryTypeAverage: '等额中奖',
      noLiveTips: '暂不支持直播前发起条件抽奖',
    },

    notice: {
      addLink: '添加链接',
      enterNotice: '请输入公告内容',
      publish: '确认发布',
      publishNew: '发布新公告',
      confirmDelTips: '删除后无法恢复，确认删除？',
      publishSuccessfullyTips: '发布成功',
      linkAddr: '链接地址',
      linkTxt: '链接文字',
      defaultLinkAddr: '默认使用链接地址',
      edit: '修改',
      add: '添加'
    },

    questionnaire: {
      rePublishErrorReason: '重发失败原因：',
      judgeQuestionErrorTip: '判断题只有一个正确答案',
      longTextMod1: '长文本模式',
      longTextMod2: '长文本模式：题目以及选项支持1000以内的文字输入',
      longTextMod3: '正常模式：题目以及选项仅支持500以内的文字输入',
      publishOrRecall: '发布/回收时间：',
      auto: '定时',
      copy: '复制问卷',
      advancePushTip: '是否要立即发出问卷',
      editQnTip: '编辑时将先暂停定时发送，是否继续编辑？',
      autoTip: '问卷发布和回收时间可自由设置',
      manual: '手动',
      manualTip: '需人工控制发布和回收时间 发布72小时后自动回收',
      recallTime: '选择回收时间',
      selectTime: '选择发布时间',
      noAnswer: '当前问卷没有设置正确答案',
      titleNotEmptyTips: '问卷名称不能为空',
      titleMaxLenTips: '问卷名称最长不超过',
      characters: '个中文',
      contentNotEmptyTips: '题目内容不能为空',
      contentMaxLenTips: '问卷题目最长不超过',
      scoreNotEmptyTips: '得分题分数不能为空',
      enterAnswer: '请提交题目的答案',
      optionContentNotEmptyTips: '选项内容不能为空',
      optionContentMaxLenTips: '选项内容最长不超过',
      questionsNotEmptyTips: '题目数量不能为空',
      confirmDiscardTips: '放弃编辑后问卷将无法恢复，确认放弃？',
      score: '分',
      respond: '已回收',
      count: '份',
      stop: '停止问卷',
      edit: '编辑',
      sendResult: '发送结果',
      publish: '发布问卷',
      publish2: '发布',
      recall: '回收',
      republish: '再次发布',
      downloadResult: '下载结果',
      enterOptions: '请输入选项内容',
      questionType: '题型:',
      oneChoiceLabel: '单选',
      multipleChoiceLabel: '多选',
      qaLabel: '问答',
      required: '必填',
      scoringQuestions: '得分题',
      enterTitle: '请输入题目内容',
      confirmDelTips: '删除后将无法恢复，确认删除？',
      name: '名称:',
      enterName: '请输入问卷名称',
      addQuestion: '添加题目',
      discardEditing: '放弃编辑',
      notExceed: '单份问卷不可超过',
      questionCount: '道题',
      create: '新建',
      newQuestionnaire: '新建问卷',
      downloadTemplate: '下载模板',
      discard: '放弃',
      continueEditing: '继续编辑',
      ongoingTips: '当前有问卷正在进行，请先前往终止',
      toStop: '去终止',
      notCreated: '尚未创建问卷',
      sent: '已发送',
      terminated: '已终止',
      publishTips: '停止问卷即可查看问卷数据(默认72小时后自动停止)',
      person: '人',
      totalSubmission: '共提交',
      noLive: '请先开始直播，再发起问卷',
      privacyTips: '开启后，观众要同意您设置的隐私声明，才可上传问卷。',
      noList: '暂无问卷列表',

      privacyContentEmptyTips: '隐私声明内容不能为空',
      excelImport: '通过Excel导入',
      excelDown: '下载Excel模板',
      publishing: '发布中',
      end: '已结束',
      waitSend: '待发布',
      stopLabel: '停止',
      publishLabel: '发布',
      rePublishLabel: '重发',
      editLabel: '编辑',
      dataLabel: '数据',
      sendResultLabel: '发送结果',
      exportDataLabel: '导出数据',
      previewQuestionnaire: '预览问卷',
      deleteQuestionnaire: '删除问卷',
      moreLabel: '更多',
      loadingMore: '加载更多中',
      exitPreview: '退出预览',
      previewLabel: '预览',
      titleLabel: '标题：',
      titlePlaceholder: '请输入标题',
      description: '描述：',
      entryDescription: '请输入描述',
      optionLabel: '选项',
      entryOptionLabel: '请输入选项',
      setAsCorrectAnswer: '设为正确答案',
      addOption: '添加选项',
      addLabel: '新增标签',
      scoreScopeTip: '得分范围无法交集',
      setAsScoringQuestion: '设为得分题',
      changeToNumericScoring: '改为数字打分',
      changeToNumericScoringTip: '五星评价：Excel统计时折算成10分，1星代表2分',
      numericScoring: '数字打分：10分制',
      single: '[单选]',
      multiple: '[多选]',
      judgement: '[判断]',
      singleChoice: '单选题',
      multipleChoice: '多选题',
      fillInTheBlank: '问答题',
      judgementQuestions: '判断题',
      starRatingQuestions: '评星题',
      multipleScores: '多项得分题',
      questionnaireName: '问卷名称',
      questionnaireDesc: '问卷简介',
      peopleNumLabel: '人数',
      proportion: '占比',
      correctAnswer: '正确答案',
      sendResultsToParticipants: '给学员发送结果',
      setPrivacyStatement: '设置隐私声明',
      privacyStatementVolume: '如：我已阅读同意《隐私声明》',
      questionnaireReceiveCount: '共 <span>{total}</span> 份问卷',
      questionnaireFillingVolume: '填写量 <span>{total}</span> 份',
      lotteryFillingVolume: '<span>{total}</span>人已答题，<span>{correct}</span>人答对',
      noPushed: '未推送',
      commonlyUsed: '常用',
      privacyAgreement: '隐私协议',
      privacyOpenStatues: '已开启',
      chooseStarTip1: '1星不满意，2星一般满意，3星感觉良好，4星感觉不错，5星非常优秀',
      chooseStarTip2: '1-10,选出你心中最佳的分数',
      youName: '你的姓名',
      tutorComments: '讲师评价',
      otherSuggestions: '其他建议',
      optionsMax: '选项最多不超过',
      labelMax: '标签最多不超过',
      questionEmpty: '点击左侧“问题类型”，可快速添加题目',
      questionTypeLabel: '题型',
      editingPopupTip: '有编辑未保存，确认退出吗？',
      sendResultErrorTip: '问卷结果只能发送一次，不要重复发送哦',
      sendResultErrorTip2: '问卷暂无人提交',
      autoPublishTime: '<span>{day}</span>天<span>{hour}</span>时<span>{min}</span>分<span>{second}</span>秒后自动发布',
      autoEndTime: '<span>{day}</span>天<span>{hour}</span>时<span>{min}</span>分<span>{second}</span>秒后自动结束',
      publishStartTime: '发布时间',
      publishEndTime: '回收时间',

      minutes: '分钟',
      chooseQuestionnaireType: '请选择问卷类型',
      surveyQuestionnaireType: '调研收集',
      classQuestionnaireType: '随堂考试',
      surveyLabel: '调研',
      voteLabel: ' 投票',
      evaluateLabel: '评价',
      examinationLabel: '考试榜单',
      limitedLabel: '限时作答',
      scoreLabel: '分数设置',
      questionnaireTime: '答题时间',
      questionnaireTotalScore: '题目总分：',
      questionnaireMaxScore: '题目最高分：',
      questionnaireMinScore: '题目最低分：',
      scoreUnit: '分',
      createOrigin: '创建于',
      rankLabel: '排行',
      nicknameLabel: '昵称',
      answerCountLabel: '答题数',
      correctCountLabel: '正确数',
      correctPercentageLabel: '正确率',
      consumingLabel: '总耗时',
      totalScoreLabel: '总得分',
      sendRankToParticipants: '给学员发送排行榜',
      sendRankTip: '（未设置得分的题目，不计入排行榜中）',
      rankNoList: '暂无排行榜',
      statisticsTip1: '查看结果只展示3条内容，导出数据可查看全部。',
      resultTab: '数据结果',
      rankTab: '排行榜',
      sendRankErrorTip: '问卷排行榜生成中，请稍后操作',
      moreThanMaxScoreTip: '试卷总分不得超过 {score} 分',
      hasNotSetScoreQuestion: '有未设置得分的题目',
      scoreNotInRange: '分数只能在最低与最高分数范围',
      scoreNotAscending: '数字只能升序',
      scoreNotAscendingAndMixed: '数字只能升序而且分数范围不能交集',
      checkDefaultBadwordTitle: '问卷标题',
      checkDefaultBadwordDesc: '问卷简介',
      checkDefaultBadwordQuestionTitle: '题目标题',
      checkDefaultBadwordQuestionDesc: '题目描述',
      checkDefaultBadwordQuestionOptions: '题目选项',
      checkDefaultBadwordPrivacyAgreement: '隐私协议',
      checkAnswerTips: '请在选项后方勾选正确答案'
    },

    // 抽奖问卷
    lotteryQn: {
      select: '选择',
      delete: '删除',
      lotterySuccess: '抽奖成功',
    },

    checkIn: {
      entryDuration: '请输入时长',
      entryTips: '请输入提示和时长',
      startTips: '各位同学开始签到了',
      onlineNum: '在线人数',
      person: '人',
      checkInNum: '签到人数',
      rate: '签到率',
      clickToDownload: '点击下载',
      nickname: '用户昵称',
      status: '签到状态',
      attendedList: '已签到名单',
      absentList: '未签到名单',
      noRecords: '暂无签到记录',
      backToHistory: '返回历史签到',
      sendResult: '发送结果',
      close: '关闭窗口',
      timeRange: '时间范围',
      search: '搜索',
      downloadRecords: '下载记录',
      checkedInTime: '签到时间',
      checkedInNum: '签到人数',
      checkedInRate: '签到率',
      operation: '操作',
      detail: '查看详情',
      noData: '暂无数据',
      enterTimeRange: '请选择时间范围进行查询数据',
      return: '返回',
      seconds: '秒',
      secondsShorthand: '秒',
      minutes: '分',
      hours: '小时',
      tips: '提示语',
      duration: '签到时长',
      historyRecords: '签到记录',
      startCheckedIn: '开始签到',
      currentlyPerson: '当前签到人数',
      stopCheckIn: '结束签到',
      type: '签到方式',
      immediateCheckin: '立即签到',
      scheduledCheckin: '定时签到',
      startCheckinTime: '开始签到时间',
      selectDateTime: '选择日期时间',
      publish: '发布',
      delete: '删除',
      delayTimeTip: '请选择开始签到时间',
      notLessCurrentTime: '开始时间不能小于当前时间',
      error501: '时间区间存在重叠或时间区间间隔小于5秒，不允许添加',
      error502: '时间区间存在重叠，不允许添加',
      error503: '一个频道最多只能添加10个定时签到',
      soundEffects: '提示音效',
      on: '开启',
      off: '关闭',
      audition: '试听'
    },

    // 问答
    qa: {
      checkAll: '查看全部',
      checkOnlyPrivate: '只看私聊',
      askLabel: '问 : ',
      answerLabel: '答 : ',
      opHandleLater: '暂不处理',
      opCollapse: '收起',
      opReply: '回复',
      statusPrivate: ' (私)',
      sessionAll: '全部场次',
      status0: '未回复',
      status1: '暂不处理',
      status10: '已回复',
      statusAll: '全部',
      tipsLoading: '加载中……',
      tipsNoData: '暂无数据哦~',
      tipsBottom: '已经到底啦~',
      tipsLoadError: '加载出错啦~',
      tipsMsgSent: '消息已发送',
      tipsParamsErr: '请求参数错误',
      tipsNet5xx: '请求服务器时发生错误',
      plhReply: '请填写您的回答',
      chkPrivate: '私聊',
      opSend: '发送',
      pictureReply: '图片回复',
    },

    timer: {
      timer: '计时器',
      start: '开始计时',
      revoke: '撤回',
      continue: '继续',
      pause: '暂停',
      timeout: '可超时',
      timeoutLabelCan: '(可超时)',
      timeoutLabelBeen: '(已超时)'
    },

    enrollLottery: {
      address: '地址：',
      enterAddress: '请输入您的收件地址',
      onlineEnabled: '是否在线',
      onlineTip: '是否过滤非在线人员',
      userId: '用户ID',
      name: '姓名：',
      enterName: '请输入您的真实姓名',
      phoneNum: '手机号码',
      enterPhoneNum: '请输入您的手机号码',
      exceedOnline: '当前在线人数可能少于设置的中奖人数，是否继续抽奖？',
      allOnlineUsers: '全体在线用户',
      missedUsers: '未中奖用户',
      congratulationTips: '恭喜以下幸运儿抽中',
      copyList: '复制名单',
      winnersList: '中奖名单',
      noNoneWon: '本轮无人中奖',
      allWon: '参与用户均已中过奖',
      continue: '继续抽奖',
      end: '结束抽奖',
      prizeName: '奖品名称：',
      enterPrizeName: '请输入奖品名称',
      participatingUsers: '参与用户：',
      winnersNum: '中奖人数：',
      enterPositiveInteger: '请输入正整数',
      presetWinning: '预设中奖：',
      quickSearch: '输入用户昵称可快速搜索',
      moreSettings: '更多设置',
      collection: '中奖用户信息采集',
      addCustomOptions: '添加自定义选项',
      custom: '自定义：',
      title: '标题',
      enterDescription: '请输入您的描述文案',
      start: '发起抽奖',
      exceedWinners: '预设名单超过设置的中奖人数',
      history: '历史记录',
      referenceName: '参考名称',
      grandPrize: '特等奖',
      firstPrize: '一等奖',
      secondPrize: '二等奖',
      thirdPrize: '三等奖',
      startLiveFirst: '请先直播再发起抽奖',
      localList: '本地名单：',
      enrollList: '报名参与：',
      uploadList: '上传名单',
      changeList: '更换名单',
      uploading: '上传中...',
      downloadTemplate: '下载模板',
      enrollStart: '报名开始：',
      enrollEnd: '报名截止：',
      autoEndLottery: '截止后自动开奖',
      filterWinner: '过滤已中奖观众',
      save: '保存',
      cancel: '取消',
      stop: '终止',
      deleteText: '确定要删除这条记录吗',
      stopText: '确定要终止抽奖吗',
      localListTitle: '名单管理',
      enrollListTitle: '报名观众名单',
      downloadList: '下载名单',
      enrollNoData: '暂无观众报名',
      return: '返回',
      launching: '发起中',
      checkDefaultBadwordPrizeName: '奖品名称',
      checkDefaultBadwordCustom: '自定义',
    },

    redpacket: {
      normalRedpacket: '普通红包',
      passwordRedpacket: '口令红包',
      redpacketRain: '红包雨',
      redpacketPwd: '红包口令',
      enterRedpacketPwd: '请输入红包口令',
      currently: '当前',
      personOnline: '人在线',
      currentlyLuckPacket: '当前为拼手气红包',
      currentlyEquallyPacket: '当前为等分红包',
      changeEquallyPacket: '改为等分红包',
      changeLuckPacket: '改为拼手气红包',
      putMoney: '塞钱',
      putScore: '发起',
      connectGM: '请联系客服开通该功能',
      toast1: '最多设置{t}元',
      toast2: '最多设置{t}个',
      pwdTipDefault: '无',
      recpackTipEmpty: '请管理员绑定公众号使用',

      statusNoSend: '未发放',
      btnBack: '返回',
      btnConfirm: '立即发放',
      sendSuccess: '红包发放成功',
      preRedpacketMsg: '提前在后台设置的红包',
      sendOtherRedpacket: '发放其他红包',
      preRedpacketTypeTable: '红包类型',
      preRedpacketName: '红包名称',
      preRedpacketType: '红包类型：',
      preRedpacketAccount: '红包金额：',
      preRedpacketQuantity: '红包数量：',
      preRedpacketStatus: '红包状态：',
      preRedpacketCoverText: '祝福语：',
      amountQuantity: '金额/数量',
      sendMethod: '发放方式',
      state: '状态',
      operation: '操作',
      luckyEnvelope: '拼手气',
      equallyEnvelope: '等分',
      immediateRelease: '立即发放',
      countdownMinute: '倒计时{minutes}分钟',
      sendBtn: '立即发送',
      returnText: '返回预设红包',
      detail: '详情',
      preRedEnvelopes: '预设红包',
      preRedpacketUnissued: '未发放',
      preRedpacketAllotRandom: '拼手气',
      preRedpacketAllotAverage: '等分',
      preYuan: '元',
      preYuan2: '元',
      preGe: '个',
      preGe1: '个共',
      preGe2: '个各',
      preExpiredTimeText: '，{date}过期',
      choosePreRedpacket: '选择红包',

      // 预设红包页面 弹窗title
      preRedpacketTitle: '选择红包',
      // 发红包成功 弹窗title
      sendRedpacketSuccessTitle: '发送成功',
      // 创建红包页 弹窗title
      createRedpacketTitle: '红包',
      // 预设红包详情 弹窗title
      preRedpacketDetailTitle: '红包详情',
      preRedpacketPwd: '红包口令：',
      prePwdTip: '口令提示：',
      canUsePoint: '当前可用{unit}：',
      payFailTip: '获取支付状态失败，请重试',
      alipayPwdRedpacket: '支付宝口令红包',
      alipayPwdTip: '请在支付宝搜索“红包”并创建口令红包后将对应内容回填至下方',
      alipayPwdPlaceholder: '请输入支付宝创建的口令',
      alipayPwdExceedTip: '如发出个数超出支付宝预设个数则有部分用户无法领取现金',
      alipayPwdTip1: '每条口令每个用户只能在支付宝领取一次',
      alipayPwdError: '口令格式不正确',
      taost3: '红包个数不可小于',
      atleast1: '金额大于0.01元',
      qnRedpacketSettingLabel: '题目配置：',
      questionTitle: '瓜分条件',
      questionAtLeast: '至少答对',
      questionAtLeastPlaceholder: '需填入≥1的数字',
      questionUnit: '题',
      changeQn: '更改',
      chooseQn: '选择题目',
      redpacketQN: '答题红包',
      qnTotal: '(共{total}题)',
      pleaseSelectQn: '请选择题目',
      chooseQnMaxError: '正确答案数量超过题目数',
      pleaseEntryCorrectAnswer: '请输入正确答案数量',
      redpacketQNGreet: '参与答题，领取红包'
    },

    redpacketRain: {
      time: '时间',
      redpacketId: '红包ID',
      send: '发送',
      account: '红包金额',
      handleFetch: '领取详情',
      plessQrcode: '请使用微信扫一扫二维码完成支付',
      paySuccess: '支付成功',
      payFail: '支付失败，请重试',

      luckyRedEnvelope: '拼手气红包',
      divideRedpackets: '等分红包',
      total: '总金额',
      totalPoint: '总{unit}',
      cny: '元',
      score: '积分',
      atleast: '金额大于1元',
      atleastPoint: '{unit}大于1个',
      pleaseEntryMoney: '请输入',
      unit: '元',
      quantity: '红包数量',
      enterNumber: '请输入红包个数',
      ge: '个',
      coverText: '祝福语',
      pwdTip: '口令提示',
      enterPwdTip: '请输入口令提示',
      bestWishes: '恭喜发财，大吉大利',
      putRedpacket: '塞进红包',

      sendCash: '发现金',
      sendPoint: '发{unit}',
      sendAlipayPassword: '发支付宝口令',

      randomTip1: '金额较大时，可能影响观众提现进度，请联系销售。',
      randomTip2: '抢红包仅能在微信内进行，未领取的红包将于24小时后发起退款。',
      randomTip3: '需向微信、银行支付通缴纳2%的交易手续费。',
      randomTip4: '观众获取红包的概率不是100%，需拼运气获得。',

      identicalRainTip1:
        '金额较大时，可能影响观众提现进度，请联系销售。',
      identicalRainTip2:
        '抢红包仅能在微信内进行，未领取的红包将在24小时后发起退款。',
      identicalRainTip3: '需向微信、银行支付缴纳2%的交易手续费。',
      identicalRainTip4: '观众获取红包的概率不是100%，需拼运气获得。',

      identicalOtherTip1:
        '金额较大时，可能影响观众提现进度，请联系销售。',
      identicalOtherTip2:
        '抢红包仅能在微信内进行，未领取的红包将在24小时后发起退款。',
      identicalOtherTip3: '需向微信、银行支付缴纳2%的交易手续费。',
      identicalOtherTip4: '观众获取红包的概率不是100%，需拼运气获得。',

      pointTip1: '用户领取红包后，可能会存在延后到账的情况，具体延后时间可咨询管理员。',

      taost1: '总金额不可大于',
      taost2: '总金额不可小于',
      taost3: '塞钱进红包失败请重试',
      toast4: '最低平分金额大于等于 {t} 元',
      toast5: '当前可用{unit}为 ',
      toast6: '红包数量需要小于可用{unit}数',
      toast7: '总{unit}不可小于',
      toast8: '最低平分{unit}大于等于 {t}',
      taost9: '总{unit}不可小于',

      sendSuccess: '发送成功',

      handleFareTip1: '实发金额包含2%交易手续费',
      handleFareTip2: '实发金额=应发金额/0.98',

      singlePrice: '单个金额',
      singlePoint: '单个{unit}',
      singlePricePla: '金额大于0.01',
      plesseEntry: '请输入',
      ping: '拼',
      deng: '等',
      countDown: '倒计时：',
      distribution: '发放方式：',
      waitDelay: '请等待上一个倒计时红包结束',
      beforeGet: '分钟后可以领取。',
      immediate: '立即发放',
      countdownDistribution: '倒计时发放',
      seeExample: '查看示例',
      watchPageExample: '观看页示例',
      minDelayLimit: '倒计时最小为',
      maxDelayLimit: '倒计时最大为',
      minutes: '分钟'
    },

    pushCard: {
      push: '推送',
      cancelPush: '取消推送',
      pushCardStyle: '卡片样式',
      title: '标题',
      link: '跳转链接',
      openType: '弹出方式',
      entryImg: '卡片入口',
      pushImmediate: '推送后立即弹出',
      pushDelay: '观看{num}{unit}后弹出',
      minutes: '分钟',
      seconds: '秒',
      cardEntry: '卡片入口',
      entryOpen: '开启',
      entryClose: '关闭',
      confirmTips: '当前有正在使用的卡片，推送后，会替换掉当前正在使用的卡片',
      pushing: '正在使用',
      lastTime: '上次推送'
    },

    productPush: {
      pushSuccess: '推送成功',
      pushFailed: '推送失败，请重新尝试',
      pushing: '正在推送',
      productNumber: '号商品',
      canPushAfterTime: '后可重新推送',
      openSuccess: '开启成功',
      closeSuccess: '关闭成功',
      onShelfSuccess: '上架成功',
      offShelfSuccess: '下架成功',
      onlyViewerVisibleCanPush: '请先打开商品列表后再推送',
      productHasBeenDeleted: '该商品已删除，请刷新重试'
    },

    bulletin: {
      new: '发布新公告',
      return: '返回',
      image: '图片',
      issuedOn: '发布于',
      today: '今天 ',
      yesterday: '昨天 ',
      sendAgain: '再发一次',
      edit: '编辑',
      delete: '删除',
      latest: '最新发布',
      announcementPlaceholder: '请输入公告',
      topLabel: '置顶公告',
      topTip: '公告发布后，观众侧将置顶该公告',
      popLabel: '发布后弹窗提示',
      popTip: '公告发布后，观众侧将主动弹出公告弹窗',
      publish: '发布',
      cancel: '取消',
      sure2Delete: '确定删除吗',
      unrecoverable: '删除后不可恢复',
      confirm: '确定',
      sure2PublishAgain: '确定再次发布吗',
      publishedSuccessfully: '发布成功',
      publishingFailedfully: '发布失败',
      emptyList: '公告列表为空',
      notEmpty: '请输入公告内容',
      picSizeExceed2mb: '图片大小不得超过2MB',
      contentExceed: '已超过输入上限，请删减部分内容',
      checkDefaultBadwordTitle: '公告',
      imageReviewFailed: '图片审核不通过',
    },

    product: {
      loading: '加载中',
      loadFail: '加载失败，请刷新重试',
      refresh: '刷新',
      title: '商品库',
      switchTitle: '观众显示商品列表',
      noneProduct: '暂无商品',
      noMore: '没有更多了',
      confirm1: '确定',
      confirm2: '吗？',
      open: '开启',
      close: '关闭',
      cancel: '取消',
      confirm3: '开启后观众端将显示商品列表',
      confirm4: '关闭后观众端将不显示商品列表',
      free: '免费',
      grounding: '上架',
      remove: '下架',
      push: '推送',
      tips1: '共 ',
      tips2: ' 件商品',
    },
  }
};
