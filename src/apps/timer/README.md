# Timer 计时器

## 引入组件

```js
import Timer from '@polyv/interactions-launch-sdk-ui-default/lib/Timer';
```



## 代码示例

```vue
<template>
	<button v-if="isTeacher" @click="timerVisible = true">计时器</button>
  <div id="pane" class="c-interactions-pane">
    <timer
      v-if="timerCore"
      :lang="lang"
      :visible="timerVisible"
      :main-area-id="'pane'"
      :position="isTeacher ? 'center' : 'bottomLeft'"
      :is-teacher="isTeacher"
      :core="timerCore"
      @close="handleTimerClose"
      @update-visible="timerVisible = true"
    />
  </div>
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import Timer from '@polyv/interactions-launch-sdk-ui-default/lib/Timer';

export default {
  components: {
    Timer,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',
      // 计时器 SDK 实例
      timerCore: null,
      timerVisible: false,
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.timerCore = new InteractionsLaunchSDK.Timer();
  },
  methods: {
    // 关闭计时器需要二次确认
    handleTimerClose(isOngoing) {
      if (!isOngoing) {
        this.timerVisible = false;
        return;
      }
      if (window.confirm(this.$t('timerCloseTips'))) {
        this.timerVisible = false;
      }
    },
  }
};
</script>
```



## Attributes

| 属性名     | 类型          | 默认值  | 含义                                                         |
| ---------- | ------------- | ------- | ------------------------------------------------------------ |
| core       | object        | null | 计时器 SDK 实例                                              |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |
| canStart | boolean | true | 当前是否可以发起互动。 |
| cantStartTips | string | '当前不可以发起互动' | 如果当前不可以发起互动，点击发起按钮时的提示文案。 |
| visible    | boolean       | false   | 是否显示计时器弹窗                                           |
| isTeacher  | boolean       | false   | 当前用户是否为讲师身份。只有讲师身份具有发起计时器的权限。                                       |
| mainAreaId | string        | '' | 定位上下文元素的唯一 id。计时器弹窗将会相对于该定位上下文进行定位。 |
| position | string | 'center' | 计时器弹窗相对于定位上下文的显示位置。取值范围：<br>'center' 上下左右居中<br>'bottomRight' 右下角对齐<br>'bottomLeft' 左下角对齐<br>'topRight' 右上角对齐<br>'topLeft' 左上角对齐 |
| zIndex     | number        | 101     | 计时器弹窗样式的 z-index                                     |



## Events

| 事件名         | 说明                                                 | 回调参数类型 | 回调参数含义           |
| -------------- | ---------------------------------------------------- | ------------ | ---------------------- |
| close          | 点击弹窗关闭按钮时触发该事件。关闭弹窗后将停止计时。 | boolean      | 是否还在进行计时。     |
| update-visible | 有正在进行的计时器时或计时器结束时会触发该事件。                   | boolean      | 当前弹窗是否应该可见。 |
