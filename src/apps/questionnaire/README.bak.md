# Questionnaire 问卷


## 引入组件

```js
import Questionnaire from '@polyv/interactions-launch-sdk-ui-default/lib/Questionnaire';
```



## 代码示例

```vue
<template>
	<!-- 建议弹窗宽度为 800px，弹窗内容高度为 550px -->
  <el-dialog
    :custom-class="'c-interactions-dialog c-interactions-dialog__h600'"
    width="800px"
    title="问卷"
    :visible.sync="questionnaireVisible"
    :close-on-click-modal="false"
  >
    <Questionnaire
      :lang="lang"
      :core="questionnaireCore"
      :result-id="questionnaireResultId"
    />
  </el-dialog>
</template>

<script>
import Questionnaire from '@polyv/interactions-launch-sdk-ui-default/lib/Questionnaire';
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    Questionnaire,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',

      questionnaireCore: null,
      questionnaireVisible: false,
      // 需要显示结果的题目 id
      questionnaireResultId: '',
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.questionnaireCore = new InteractionsLaunchSDK.Questionnaire();
    // 'show-result' 事件被触发时主动打开弹窗显示问卷结果
    this.questionnaireCore
        .on('show-result', ({ questionnaireId }) => {
          this.questionnaireVisible = true;
          this.questionnaireResultId = questionnaireId;
        });
  }
}
</script>
```



## Attributes

| 属性名        | 类型          | 默认值               | 含义                                               |
| ------------- | ------------- | -------------------- | -------------------------------------------------- |
| core          | Object        | null                 | 问卷 SDK 实例                                      |
| resultId      | String        | ''                   | 需要显示结果的问卷 id                              |
| lang          | 'zh_CN'\|'en' | 'zh_CN'              | 语言包类型                                         |
| canStart      | Boolean       | true                 | 当前是否可以发起互动。                             |
| cantStartTips | String        | '当前不可以发起互动' | 如果当前不可以发起互动，点击发起按钮时的提示文案。 |
| sendRoomList | Array        | []                    | 显示发送范围人群数据 |
| useSeminarChatApi | Boolean | false | 是否使用研讨会问卷接口 |
| useInteractionsToast | Boolean | true | 是否使用互动默认Toast |
| sendRoomConfirmText | String | '立即发布' | 发送范围人群确认按钮文案 |

##methods
| 方法名 | 参数 | 说明 |
| ------------- | ------------- | --------------------|
| change-send-room-lis  | sendRoomList      |   sendRoomList 发生变化  |
| alert-message | string | 不使用互动默认Toast,抛出对应提示语 |

## sendRoomList
| 属性名        | 类型          | 默认值         |  可选值      | 含义                                               |
| ------------- | ------------- | --------------------| ------------- | --------------------------------------------------   |
| type            | String      |    ——   | attendee / viewer | 大房间/所有分组/观众                          |
| label            | String      |  ——     | ——  |     选项名                     |
| checked          | Boolean     | false | false / true | 是否选中                                                                 |
| disabled         | Boolean     | false | false / true | 是否禁用                                                                 |
