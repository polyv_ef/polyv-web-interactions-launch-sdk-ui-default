import Vue from 'vue';
import Main from './main.vue';
const MessageConstructor = Vue.extend(Main);

let seed = 0;
let instance;
const instances = [];

const Message = (parentNode) => {
  const msg = function(options = {}) {
    if (typeof options === 'string') {
      options = {
        message: options
      };
    }
    const id = 'message_' + seed++;
    msg.closeAll();
    instance = new MessageConstructor({
      data: options
    });
    if (options.parentNode) {
      parentNode = options.parentNode;
    } else {
      parentNode = document.body;
    }
    instance.id = id;
    instance.$mount();
    parentNode.appendChild(instance.$el);
    instance.visible = true;
    instances.push(instance);
    return instance;
  };

  msg.closeAll = function() {
    for (let i = instances.length - 1; i >= 0; i--) {
      instances[i].close();
    }
  };
  return msg;
};

export default Message;
