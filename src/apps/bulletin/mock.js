/* eslint-disable */ 
export const mockData = [
  {
    content: 'POLYV保利威（曾用名：保利威视）是广州易方信息科技有限公司旗下的基于视频云计算的视频云点播、云直播、云课堂和视频私有云等多款视频POLYV保利POLYV保利威（曾用名：保利威视）是广州易方信息科技有限公司旗下的基于视频云计算的视频云点播、云直播、云课堂和视频私有云等多款视频POLYV保利POLYV保利威（曾用名：保利威视）是广州易方信息科技有限公司旗下的基于视频云计算的视频云点播、云直播、云课堂和视频私有云等多款视频POLYV保利',
    createTime: Date.now(),
    timestamp: Date.now(),
    id: '0001',
    isPop: false,
    isTop: true,
    user: {
      actor: '讲师',
      nick: '小橙子',
      pic: 'https://demo2.polyv.net/linxiaoru/2020092402.jpeg',
      userId: '1362678',
      userType: 'teacher',
    }
  },
  {
    content: '广州易方信息科技有限公司旗下的基于视频云计算的视频云点播。',
    createTime: Date.now(),
    timestamp: Date.now(),
    id: '0002',
    isPop: true,
    isTop: false,
    user: {
      actor: '助教',
      nick: '卡卡',
      pic: 'https://demo2.polyv.net/linxiaoru/2020092402.jpeg',
      userId: '11362678',
      userType: 'assistant',
    }
  },
  {
    content: '广州易方信息科技有限公司旗下的基于视频云计算的视频云点播。123123',
    createTime: Date.now(),
    timestamp: Date.now(),
    id: '0003',
    isPop: false,
    isTop: false,
    user: {
      actor: '助教',
      nick: '啦啦',
      pic: 'https://demo2.polyv.net/linxiaoru/2020092402.jpeg',
      userId: '21362678',
      userType: 'assistant',
    }
  },
  {
    content: '广州易方信息科技有限公司旗下的基于视频云计算的视频云点播。aaa',
    createTime: Date.now(),
    timestamp: Date.now(),
    id: '0004',
    isPop: false,
    isTop: false,
    user: {
      actor: '助教',
      nick: 'miemie',
      pic: 'https://demo2.polyv.net/linxiaoru/2020092402.jpeg',
      userId: '31362678',
      userType: 'assistant',
    }
  },
  {
    content: '广州易方信息科技有限公司旗下的基于视频云计算的视频云点播。bbb',
    createTime: Date.now(),
    timestamp: Date.now(),
    id: '0005',
    isPop: false,
    isTop: false,
    user: {
      actor: '助教',
      nick: '恰恰',
      pic: 'https://demo2.polyv.net/linxiaoru/2020092402.jpeg',
      userId: '41362678',
      userType: 'assistant',
    }
  },
];
