const path = require('path');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const webpack = require('webpack');
const { transpileDependencies, alias, version } = require('./build-config');

const ruleConfig = {
  vue: {
    test: /\.vue$/,
    use: ['vue-loader']
  },
  scss: {
    test: /\.scss$/,
    use: [
      'vue-style-loader',
      { loader: 'css-loader', options: { sourceMap: false } },
      { loader: 'postcss-loader', options: { sourceMap: false } },
      {
        loader: 'sass-loader',
        options: {
          implementation: require('sass')
        }
      },
      {
        loader: 'sass-resources-loader',
        options: {
          resources: path.resolve(__dirname, '../src/assets/styles/mixins.scss')
        }
      }
    ]
  },
  js: {
    test: /\.m?js$/,
    use: [
      'babel-loader'
    ]
  },
  css: {
    test: /\.css$/,
    use: [
      'style-loader',
      { loader: 'css-loader', options: { sourceMap: false } },
      { loader: 'postcss-loader', options: { sourceMap: false } },
    ]
  },
  assets: {
    test: /\.(png|gif|jpg|jpeg|svg|svga|ico|ttf|eot|woff|woff2|otf|mp3)$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          limit: 5120,
          outputPath: 'assets/',
          name: '[name]-[hash:8].[ext]',
          esModule: false
        }
      }
    ]
  }
};

const srcDirname = path.resolve(__dirname, '../src');
const nodeModules = path.resolve(__dirname, '../node_modules');
const ruleKeys = Object.keys(ruleConfig);
const rules = [];
ruleKeys.forEach((key) => {
  ruleConfig[key].include = [srcDirname];
  if (transpileDependencies) {
    transpileDependencies.forEach((item) => {
      if (typeof item === 'string') {
        ruleConfig[key].include.push(path.join(nodeModules, item));
      } else if (item[key]) {
        ruleConfig[key].include.push(path.join(nodeModules, item.name));
      }
    });
  }
  rules.push(ruleConfig[key]);
});

module.exports = {
  resolve: {
    extensions: ['.js', '.ts', '.mjs', '.vue', '.css', '.scss'],
    alias
  },

  module: {
    rules
  },

  plugins: [
    new VueLoaderPlugin(),
    new StyleLintPlugin({
      files: ['**/*.{vue,css,scss}']
    }),
    new ESLintPlugin({
      files: ['**/*.{vue,js}']
    }),
    new webpack.DefinePlugin({
      'APP_VERSION': JSON.stringify(version),
      'APP_BUILD_TIME': JSON.stringify(new Date().toString())
    }),
  ]
};
