import Timer from '../assets/timer';

export default {
  props: {
    /** 计时器 SDK 实例 */
    core: {
      type: Object,
      default: null,
    },
    reconnectTime: 500 // 重连后，默认等待500毫秒内是否收到starTimer事件, 判断计时器是否存在。
  },
  data() {
    return {
      isOngoing: false,
      remainingTime: 0,
      countDownTimer: null,
      isPaused: false, // 已暂停
      enableTimeOut: false, // 可超时
      socketConnected: true,
      timerConnect: null, // socket 重连后，计时器是否在运行
      requireEnd: true,
      // 超时时，是否已经重置过计时器
      isResetTimer: false,
    };
  },
  watch: {
    visible: {
      handler(newVal, oldVal) {
        if (this.isOngoing && !newVal && oldVal) {
          this.overTimer();
        }
      }
    }
  },

  computed: {
    isTimedOut() {
      return this.isOngoing && this.remainingTime < 0;
    },

    subTitle() {
      if (this.isOngoing && this.enableTimeOut) {
        return this.isTimedOut ? this.t('plvIA.timer.timeoutLabelBeen') : this.t('plvIA.timer.timeoutLabelCan');
      }
      return '';
    },

    subTitleStyle() {
      const basicStyle = {
        'padding-left': '0.5em'
      };
      if (this.isTimedOut) {
        return Object.assign({
          color: '#FF5B5B',
        }, basicStyle);
      }
      return basicStyle;
    },

    maxTimeOut() {
      const ONE_HOUR = -60 * 60;
      return this.enableTimeOut ? ONE_HOUR : 0;
    }
  },

  methods: {
    afterCoreInited() {
      this.bindCoreListeners();
    },

    bindCoreListeners() {
      this.core.bindSocketIOEvents();
      this.core
        .on('start', this.onStart)
        .on('stop', this.onStop)
        .on('end', this.onOver)
        .on('restart', this.onRestart)
        .on(this.core.socketIOEvents.DISCONNECT, () => {
          this.timerConnect = false;
        })
        .on(this.core.socketIOEvents.CONNECT, async () => {
          // 需要在收到 START_TIMER 事件之后，才能确定该房间是否有计时器
          setTimeout(() => {
            if (!this.timerConnect) {
              this.onOver();
            }
          }, this.reconnectTime);
        });
    },

    unbindCoreListeners() {
      this.core
        .off('start', this.onStart)
        .off('stop', this.onStop)
        .off('end', this.onOver)
        .off('restart', this.onRestart);
    },

    setModalPosition(modal) {
      const mainAreaEl = document.getElementById(this.mainAreaId);
      const mainRect = mainAreaEl && mainAreaEl.getBoundingClientRect();

      return {
        center: {
          top: mainRect.bottom - mainAreaEl.clientHeight / 2 - modal.clientHeight / 2,
          left: mainRect.right - mainAreaEl.clientWidth / 2 - modal.clientWidth / 2,
        },
        bottomRight: {
          top: mainRect.bottom - modal.clientHeight,
          left: mainRect.right - modal.clientWidth,
        },
        bottomLeft: {
          top: mainRect.bottom - modal.clientHeight,
          left: mainRect.left,
        },
        topRight: {
          top: mainRect.top,
          left: mainRect.right - modal.clientWidth,
        },
        topLeft: {
          top: mainRect.top,
          left: mainRect.left,
        }
      }[this.position];
    },

    handleSendRoomModalConfirm() {
      this.showSendRoomModal = false;
      this.startTimer(this.time, this.sendRoomList);
    },

    beforeStartTimer(time) {
      this.time = time;
      if (this.useSeminarChatApi && this.sendRoomList.length > 0) {
        const sendTarget = this.sendRoomList.filter(item => !!item.disabled === false);
        if (sendTarget.length > 1) {
          this.showSendRoomModal = true;
        } else {
          // 判断只有一个人群可选择，默认选中直接发送
          sendTarget[0].checked = true;
          this.startTimer(time, sendTarget);
        }
      } else {
        this.startTimer(time);
      }
    },

    async startTimer(time, sendTarget = null) {
      if (!this.canStart) {
        this.$iaMessage.default(this.cantStartTips);
        return;
      }
      // 设置发送人群
      sendTarget = this.getSendTargetParam(sendTarget);
      await this.core.start({
        time,
        enableTimeOut: this.enableTimeOut,
        sendTarget
      });
    },

    /**
     * 结束计时。
     */
    async overTimer() {
      this.clearCountDownTimer();
      await this.core.end();
      this.onOver();

      this.$refs.modal.updateBasicStyle(true);
    },

    resetTimer() {
      this.overTimer();
    },

    handleClose() {
      const isOngoing = this.isOngoing && this.remainingTime !== 0;
      this.$emit('close', isOngoing);
    },

    onOver() {
      this.isOngoing = false;
      this.remainingTime = 0;
      this.isResetTimer = false;
      this.clearCountDownTimer();
      this.$emit('update-visible', false);
    },

    onStart(data) {
      this.timerConnect = true;
      this.playSoundEffects();
      const { time, inStop: isPaused, canTimeout: enableTimeOut } = data;
      this.isOngoing = true;
      this.enableTimeOut = enableTimeOut;
      this.isResetTimer = time < 0;
      if (time > 0 || (time < 0 && this.enableTimeOut)) {
        this.remainingTime = time;
      }
      if (isPaused) {
        this.isPaused = true;
      } else {
        this.isPaused = false;
        this.initCountdown();
      }
      this.$emit('update-visible', true);
    },
    initCountdown() {
      // 清理旧定时器
      if (this.countDownTimer) {
        this.countDownTimer.stop();
        this.countDownTimer = null;
      }
      this.countDownTimer = new Timer({
        secs: Math.abs(this.remainingTime),
        mode: this.remainingTime > 0 ? 'sub' : 'add',
        cb: this.setCountDown,
      });
      this.countDownTimer.start();
    },

    onStop({ time }) {
      this.isPaused = true;
      this.remainingTime = time;
      this.clearCountDownTimer();
    },

    onRestart({ time }) {
      this.isPaused = false;
      this.remainingTime = time;
      this.initCountdown();
    },

    setCountDown() {
      if (this.remainingTime <= this.maxTimeOut || this.isPaused || !this.requireEnd) return;
      this.getRemainingTime();
      // this.clearCountDownTimer();
    },

    clearCountDownTimer() {
      if (!this.countDownTimer) return;
      this.countDownTimer.stop();
      // clearTimeout(this.countDownTimer);
      // this.countDownTimer = null;
    },

    /**
     * 获取剩余计时时间。
     */
    async getRemainingTime() {
      this.requireEnd = false;
      const status = await this.core.getTimerStatus();
      this.updateRemainingTime(status.remainingTime, status.canTimeout);
      this.isPaused = status.isStoppedStatus;
      this.requireEnd = true;
      return status;
    },
    updateRemainingTime(time, canTimeout) {
      if (!canTimeout && time < 0) {
        this.remainingTime = 0;
      } else {
        this.remainingTime = time;
        if (time <= 0 && !this.isResetTimer) {
          this.isResetTimer = true;
          this.initCountdown();
        }
      }
    },

    async changePausedStatus() {
      if (this.isPaused) {
        await this.core.restart();
      } else {
        await this.core.stop();
      }
      if (!this.isPaused) {
        this.initCountdown();
      } else {
        this.clearCountDownTimer();
      }
    },

    changeTimeOut(val) {
      this.enableTimeOut = val;
    }
  }
};
