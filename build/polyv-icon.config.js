const { defineConfig } = require('@polyv/icons-cli');

module.exports = defineConfig({
  // svg 文件目录，默认：resources
  resourcesDir: './resources',
  // 输出目录，默认：dist
  outDir: './src/icon-components',
  // 图标库类型，默认：svg，请根据类型安装对应的图标库
  libraryType: 'vue',
});
