export default {
  data() {
    // tab 红包类型 normal：普通红包 ， password：口令红包， rain: 红包雨， alipayPwd：支付宝口令红包
    const tabTypeKey = {
      normal: 'normal',
      password: 'password',
      rain: 'rain',
      alipayPwd: 'alipayPwd',
      question: 'question'
    };
    const tabType = {
      [tabTypeKey.normal]: 'OFFICIAL_NORMAL',
      [tabTypeKey.password]: 'PASSWORD',
      [tabTypeKey.rain]: 'RAIN',
      [tabTypeKey.question]: 'QUESTION',
    };
    // 红包类型：0 拼手气 1 等分
    const rpTypes = { random: 0, equal: 1 };
    // 红包支付方式
    const payTypes = { cash: 'cash', point: 'score', alipayPassword: 'alipayPassword' };
    return {
      tabType,
      rpTypes,
      tabTypeKey,
      payTypes,
    };
  }
};
