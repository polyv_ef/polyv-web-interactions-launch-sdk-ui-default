const path = require('path');
const { merge } = require('webpack-merge');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const baseConfig = require('./webpack.config');
const { devServerPort, devServerProxy, devServerHost } = require('./build-config');
const glob = require('glob');
const { getAssetsOrigin } = require('@polyv/sdk-cli/lib/utils');
const buildConfig = require('./build-config');
const {buildPkgJSON} = require("./build-package-info");

const srcPath = path.resolve(__dirname, '../src');
const distPath = path.resolve(__dirname, '../dist');
const copySrc = path.resolve(__dirname, '../public');
const publicPath = '/';

const entry = {};
const entryDirname = path.resolve(__dirname, '../src/apps');
glob.sync(
  path.join(entryDirname, '*/')
).forEach((fullPath) => {
  const temp = fullPath.split(/[\\/]+/);
  const name = temp[temp.length - 2]
    .replace(/-([a-z])/, function(match, $1) {
      return $1.toUpperCase();
    })
    .replace(/^[a-z]/, function(match) {
      return match.toUpperCase();
    });
  entry[name] = path.join(fullPath, name + '.vue');
});

const libBaseConfig = merge(baseConfig, {
  devtool: 'source-map',
  entry,
  output: {
    publicPath: getAssetsOrigin(buildConfig.env) +
      buildConfig.ossPath +
      buildConfig.version +
      '/'
  }
});

module.exports = [
  merge(libBaseConfig, {
    mode: 'production',
    output: {
      path: path.resolve(__dirname, '../distwatch/'),
      filename: 'lib/[name]/index.js',
      libraryTarget: 'commonjs2'
    },
    externals: {
      vue: 'vue',
      'element-ui': 'element-ui',
    },
    watch: true,
    plugins: [
      new CopyPlugin({
        patterns: [
          {
            from: path.resolve(__dirname, '../package.json'),
            to: path.join(__dirname, '../distwatch/', 'package.json'),
            transform(content) {
              return buildPkgJSON(content);
            }
          },
          {
            from: path.resolve(__dirname, '../README.publish.md'),
            to: path.join(buildConfig.currentLibDest, 'README.md')
          }
        ]
      }),
      // new BundleAnalyzerPlugin(),
    ],
    devServer: {
      port: 23009,
      disableHostCheck: true,
      host: devServerHost,
      hot: true,
      overlay: {
        warnings: false,
        errors: true
      },
      proxy: devServerProxy,
      https: true,
    }
  }),
];
