const path = require('path');
const glob = require('glob');
const { merge } = require('webpack-merge');
const CopyPlugin = require('copy-webpack-plugin');
const rimraf = require('rimraf');
const baseConfig = require('./webpack.config');
const buildConfig = require('./build-config');
const { buildPkgJSON } = require('./build-package-info');
const { getAssetsOrigin } = require('@polyv/sdk-cli/lib/utils');

// 打包分析
if (process.env.REPORT) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
  baseConfig.plugins.push(new BundleAnalyzerPlugin());
}
// 所有互动打包一起
const entrance = path.resolve(__dirname, '../src/entrance/entrance.js');

// 删除上次构建的文件
console.info('Clearing old files...');
rimraf.sync(path.join(path.resolve(__dirname, '../dist'), '**', '*'));
console.info('Done.');

// 找到所有要构建的入口文件
const entry = {};
const entryDirname = path.resolve(__dirname, '../src/apps');
glob.sync(
  path.join(entryDirname, '*/')
).forEach((fullPath) => {
  const temp = fullPath.split(/[\\/]+/);
  const name = temp[temp.length - 2]
    .replace(/-([a-z])/g, function(match, $1) {
      return $1.toUpperCase();
    })
    .replace(/^[a-z]/, function(match) {
      return match.toUpperCase();
    });
  entry[name] = path.join(fullPath, name + '.vue');
});

console.log('entry', entry);
console.log('entrance', entrance);
const libBaseConfig = merge(baseConfig, {
  devtool: 'source-map',
  entry: Object.assign(entry, { entrance }),
  output: {
    publicPath: getAssetsOrigin(buildConfig.env) +
      buildConfig.ossPath +
      buildConfig.version +
      '/'
  }
});

// 分别构建 commonjs 和 umd 格式的文件
module.exports = [
  merge(libBaseConfig, {
    mode: 'production',
    output: {
      path: buildConfig.currentLibDest,
      filename: 'lib/[name]/index.js',
      libraryTarget: 'commonjs2'
    },
    externals: {
      vue: 'vue',
      'element-ui': 'element-ui',
    },
    plugins: [
      new CopyPlugin({
        patterns: [
          {
            from: path.resolve(__dirname, '../package.json'),
            to: path.join(buildConfig.currentLibDest, 'package.json'),
            transform(content) {
              return buildPkgJSON(content);
            }
          },
          {
            from: path.resolve(__dirname, '../README.publish.md'),
            to: path.join(buildConfig.currentLibDest, 'README.md')
          }
        ]
      }),
      // new BundleAnalyzerPlugin(),
    ]
  }),

  merge(libBaseConfig, {
    mode: 'production',
    output: {
      path: buildConfig.currentLibDest,
      filename: 'lib/[name]/[name].umd.min.js',
      libraryTarget: 'umd',
      library: [buildConfig.libGlobal, '[name]']
    },
    externals: {
      vue: {
        commonjs: 'vue',
        commonjs2: 'vue',
        amd: 'vue',
        root: 'Vue'
      },
      'element-ui': 'element-ui',
    }
  })
];
