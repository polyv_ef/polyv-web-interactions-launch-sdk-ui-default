# CheckIn 签到

## 引入组件

```js
import CheckIn from '@polyv/interactions-launch-sdk-ui-default/lib/CheckIn';
```



## 代码示例

```vue
<template>
	<!-- 建议弹窗宽度为 560px，弹窗内容高度为 350px -->
  <el-dialog
    v-if="checkInCore"
    :custom-class="'c-interactions-dialog c-interactions-dialog__h350'"
    width="560px"
    title="签到"
    :visible.sync="checkInVisible"
    :close-on-click-modal="false"
  >
    <check-in
      :lang="lang"
      :result-id="checkInResultId"
      :core="checkInCore"
      :right="{
        sendResult: true,
        showHistoryEntry: false,
        download: false,
      }"
    />
  </el-dialog>
</template>

<script>
import CheckIn from '@polyv/interactions-launch-sdk-ui-default/lib/CheckIn';
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
// 弹窗组件可以选择其他 UI 库组件或自行开发，这里没有限制。
import { Dialog } from 'element-ui';

export default {
  components: {
    ElDialog: Dialog,
    CheckIn,
  },
  data() {
    return {
      // 中英文设置 'zh_CN'/'en'
      lang: 'zh_CN',
      checkInCore: null,
      checkInVisible: false,
      // 需要显示结果的签到 id
      checkInResultId: '',
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.checkInCore = new InteractionsLaunchSDK.CheckIn();
    this.checkInCore
      .on('show-result', ({ checkInId }) => {
        this.checkInVisible = true;
        this.checkInResultId = checkInId;
      });
  }
};
</script>
```



## Attributes

| 属性名        | 类型          | 默认值                                                     | 含义                                                         |
| ------------- | ------------- | ---------------------------------------------------------- | ------------------------------------------------------------ |
| core          | object        | null                                                       | 签到 SDK 实例                                                |
| lang | string | 'zh_CN' | 语言包类型。取值范围：<br>'zh_CN' 中文 <br>'en' 英文 |
| canStart      | boolean       | true                                                       | 当前是否可以发起互动。                                       |
| cantStartTips | string        | '当前不可以发起互动'                                          | 如果当前不可以发起互动，点击发起按钮时的提示文案。           |
| resultId      | string        | ''                                                         | 需要显示结果的签到 id                                        |
| right         | object | {sendResult: false,showHistoryEntry: true,download: true,} | 操作权限。类型定义详见[Right](#Right)。 |

### Right 对象说明
| 属性名        | 类型          | 默认值                                                     | 含义                                                         |
| ------------- | ------------- | ---------------------------------------------------------- | ------------------------------------------------------------ |
| sendResult        | boolean          | false | 表示操作“发送结果”的权限 |
| showHistoryEntry        | boolean          | true | 是否显示“历史签到记录”入口 |
| download        | boolean          | true | 是否显示下载链接。 |
