/**
 * dom 操作相关的工具函数
 */

/**
 * 判断元素A是否为元素B的子元素
 * @param {Object} obj 元素A
 * @param {Object} parentObj 元素B
 */
export function isParent(obj, parentObj) {
  while (typeof obj !== 'undefined' && obj != null && obj.tagName && obj.tagName.toUpperCase() !== 'BODY') {
    if (obj === parentObj) {
      return true;
    }
    obj = obj.parentNode;
  }
  return false;
}
