# redpacket 倒计时红包挂件


## 引入组件

```js
import redpacket from '@polyv/interactions-launch-sdk-ui-default/lib/redpacket';
```



## 代码示例

```vue
<template>
    <redpacket-countdown
      :core="redpacketCore"
    />
</template>

<script>
import * as InteractionsLaunchSDK from '@polyv/interactions-launch-sdk';
import RedpacketCountdown from '@/apps/redpacket-countdown/RedpacketCountdown';

export default {
  components: {
    RedpacketCountdown,
  },
  data() {
    return {
      redpacketCore: null,
    };
  },
  mounted() {
    // TODO: 关于初始化 InteractionsLaunchSDK 的其他设置
    this.redpacketCore = new InteractionsLaunchSDK.redpacket();
  }
};
</script>
```



## Attributes

| 属性名     | 类型          | 默认值  | 含义                                                         |
| ---------- | ------------- | ------- | ------------------------------------------------------------ |
| core       | object        |         | 抽奖 SDK 实例                                              |
| delayTime       | number        |    0     |  倒计时时间（毫秒）, 传入delayTime 不再从接口获取倒计时       |
