// 红包雨倒计时相关逻辑
export default {
  props: {
    delayTime: {
      // 倒计时时间（毫秒）, 传入delayTime 不再从接口获取倒计时
      type: Number,
      default: 0
    },
    core: {
      type: Object,
      default: null
    }
  },
  data() {
    // tab 红包类型 normal：普通红包 ， password：口令红包， rain: 红包雨
    const tabType = { normal: 'OFFICIAL_NORMAL', password: 'PASSWORD', rain: 'RAIN', question: 'QUESTION' };
    return {
      delayTimeSecond: 0, // 剩余秒数
      timer: null, // 倒计时定时器
      tabType,
      selfRedpackType: tabType.normal // 红包类型
    };
  },
  watch: {
    delayTime(val) {
      if (this.channelId) return;
      if (this.timer) clearInterval(this.timer);
      this.delayTimeSecond = val;
      this.initTimer();
    }
  },
  computed: {
    // 计算时间
    calcDelay() {
      const delayTimeSecond = Math.floor(this.delayTimeSecond);
      // let hour = 0;
      let min = 0;
      let second = 0;

      if (delayTimeSecond > 60) {
        // 是否大于零
        min = Math.floor(delayTimeSecond / 60); // 分钟
        second = Math.floor(delayTimeSecond % 60); // 秒
        // 后续可能需要小时展示
        // if (min > 60) {
        //   // 存在时
        //   // hour = Math.floor(min / 60);
        //   min = Math.floor(min % 60);
        // }
      } else if (delayTimeSecond > 0) {
        second = delayTimeSecond;
      }
      return {
        // hour: hour > 9 ? hour : '0' + hour,
        min: min > 9 ? min : '0' + min,
        second: second > 9 ? second : '0' + second
      };
    }
  },
  methods: {
    // 获取最新的定时红包状态，并计算出倒计时
    async getDelayTime() {
      let time = 0;
      try {
        const data = await this.core.getNewest();
        if (data.redpackType) {
          this.selfRedpackType = data.redpackType;
        }
        if (data && data.timeEnabled === 'Y' && data.sendTime > data.serverTime) {
          time = Math.floor((data.sendTime - data.serverTime) / 1000);
        }
        return time;
      } catch (e) {
      }
    },
    initTimer() {
      if (!this.timer) {
        let oldDate = new Date().getTime();
        this.timer = setInterval(() => {
          const newDate = new Date().getTime();
          const diffDate = Math.round((newDate - oldDate) / 1000);
          if (this.delayTimeSecond < 0) {
            clearInterval(this.timer);
            this.timer = null;
          } else {
            this.delayTimeSecond = this.delayTimeSecond - diffDate;
            oldDate = newDate;
          }
        }, 1000);
      }
    }
  },
  async mounted() {
    // 如果没有delayTime 从接口获取时间, 否则使用delayTime
    if (this.delayTime) {
      this.delayTimeSecond = this.delayTime;
    } else {
      this.delayTimeSecond = await this.getDelayTime();
    }
    if (this.delayTimeSecond > 0) {
      this.initTimer();
    }
  },
  beforeDestroy() {
    if (this.timer) clearInterval(this.timer);
  }
};
